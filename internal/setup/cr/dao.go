package cr

import (
	"context"
	"fmt"
	"gitee.com/lailonghui/vehicle-supervision-framework/pkg/asserts"
	"gitee.com/lailonghui/vehicle-supervision-framework/pkg/beans"
	"gitee.com/shiqiyue/xd-bi/internal/modules/cr/dao"
)

// 初始化dao
func DaoInit(ctx context.Context) {
	dao.ReportDbinit()
	dao.Reportinit()
	dao.ReportForminit()
	dao.ReportResultColumninit()
	dao.SqlReportinit()
	dao.ReportAggregateInit()

	// 注入
	err := beans.Populate()
	asserts.Nil(err, err)
	fmt.Println("dao init done")

}
