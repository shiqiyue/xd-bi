package cr

import (
	"context"
	"errors"
	"gitee.com/lailonghui/vehicle-supervision-framework/pkg/apms"
	"gitee.com/lailonghui/vehicle-supervision-framework/pkg/asserts"
	"gitee.com/lailonghui/vehicle-supervision-framework/pkg/loggers"
	"gitee.com/lailonghui/vehicle-supervision-framework/pkg/loggers/rabbit_zap"
	"gitee.com/lailonghui/vehicle-supervision-framework/pkg/vipers"
	"gitee.com/shiqiyue/xd-bi/configs"
	"gitee.com/shiqiyue/xd-bi/internal/pkg/instance"
	"github.com/spf13/pflag"
	"github.com/spf13/viper"
	"strings"
	"sync"
)

var (
	viperEtcdEndpointConfig string = "viper.etcd.endpoints"
	viperEtcdAppPathConfig  string = "viper.etcd.path"
	// 服务地址配置文件
	viperEtcdSrvAddressPathConfig string = "viper.etcd.srv_address_path"
	// 权限配置文件
	viperEtcdAuthPathConfig string = "viper.etcd.auth_public_path"
	// apm配置文件
	viperApmPathConfig string = "viper.etcd.apm_path"
	// 日志收集器配置文件
	viperLogCollectorPathConfig string = "viper.etcd.logger_collector_path"
)

// 配置信息初始化
// 设置环境变量 VIPER_ETCD_ENDPOINTS 和 VIPER_ETCD_PATH 覆盖默认的配置中心值
func ConfigInit(ctx context.Context) {

	// 从命令行中获取配置中心默认值
	v := viper.New()
	pflag.String(viperEtcdEndpointConfig, "10.10.20.132:2379", "etcd配置中心地址")
	pflag.String(viperEtcdAppPathConfig, "/dev/cr-srv/app.yaml", "配置文件在配置中心的路径")
	pflag.String(viperEtcdSrvAddressPathConfig, "/dev/srv-address.yaml", "服务地址配置文件")
	pflag.String(viperEtcdAuthPathConfig, "/dev/auth-public.yaml", "权限配置文件")
	pflag.String(viperApmPathConfig, "/dev/apm.yaml", "apm配置文件")
	pflag.String(viperLogCollectorPathConfig, "/dev/logger_collector.yaml", "日志收集器配置文件")

	pflag.Parse()
	err := v.BindPFlags(pflag.CommandLine)
	asserts.Nil(err, errors.New("bind pflag fail"))

	// 从环境变量取值
	v.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))
	err = v.BindEnv(viperEtcdEndpointConfig)
	asserts.Nil(err, errors.New("bind env fail"))
	err = v.BindEnv(viperEtcdAppPathConfig)
	asserts.Nil(err, errors.New("bind env fail"))
	err = v.BindEnv(viperEtcdAuthPathConfig)
	asserts.Nil(err, errors.New("bind env fail"))
	err = v.BindEnv(viperApmPathConfig)
	asserts.Nil(err, errors.New("bind env fail"))
	err = v.BindEnv(viperLogCollectorPathConfig)
	asserts.Nil(err, errors.New("bind env fail"))
	v.AutomaticEnv()
	wg := sync.WaitGroup{}
	wg.Add(1)

	// 获取配置信息，并且监听配置信息变化
	vipers.InitEtcdRemote()
	vipers.ConfigWithEtcd(func(v *viper.Viper) {
		instance.SetViper(v)
		c := configs.Config{}
		err := v.Unmarshal(&c)
		asserts.Nil(err, err)
		instance.SetConfig(&c)
		wg.Done()
		loggers.Info("应用 viper配置完毕", context.Background())
	}, func(v *viper.Viper) {
		instance.SetViper(v)
		c := configs.Config{}
		err := v.Unmarshal(&c)
		asserts.Nil(err, err)
		instance.SetConfig(&c)
		loggers.Info("应用 viper配置刷新完毕", context.Background())
	}, v.GetString(viperEtcdEndpointConfig), v.GetString(viperEtcdAppPathConfig))
	wg.Add(1)

	// 服务发现
	vipers.ConfigWithEtcd(func(v *viper.Viper) {
		instance.SetSrvAddressViper(v)
		wg.Done()
		loggers.Info("服务发现 viper配置完毕", context.Background())
	}, func(v *viper.Viper) {
		instance.SetSrvAddressViper(v)
		loggers.Info("服务发现 viper配置刷新完毕", context.Background())
	}, v.GetString(viperEtcdEndpointConfig), v.GetString(viperEtcdSrvAddressPathConfig))
	wg.Add(1)

	// 权限
	vipers.ConfigWithEtcd(func(v *viper.Viper) {
		instance.SetAuthViper(v)
		wg.Done()
		loggers.Info("权限 viper配置完毕", context.Background())

	}, func(v *viper.Viper) {
		instance.SetAuthViper(v)
		loggers.Info("权限 viper配置刷新完毕", context.Background())
	}, v.GetString(viperEtcdEndpointConfig), v.GetString(viperEtcdAuthPathConfig))
	wg.Add(1)

	// apm
	vipers.ConfigWithEtcd(func(v *viper.Viper) {
		instance.SetApmViper(v)
		c := apms.ApmConfig{}
		err := v.Unmarshal(&c)
		asserts.Nil(err, err)
		instance.SetApmConfig(&c)
		wg.Done()
		loggers.Info("apm viper配置完毕", context.Background())
	}, func(v *viper.Viper) {
		instance.SetApmViper(v)
		c := apms.ApmConfig{}
		err := v.Unmarshal(&c)
		asserts.Nil(err, err)
		instance.SetApmConfig(&c)
		loggers.Info("apm viper配置刷新完毕", context.Background())
	}, v.GetString(viperEtcdEndpointConfig), v.GetString(viperApmPathConfig))

	wg.Add(1)
	// 日志收集器
	vipers.ConfigWithEtcd(func(v *viper.Viper) {
		ctx := context.Background()
		c := rabbit_zap.LoggerCollectorConfig{}
		err := v.Unmarshal(&c)
		asserts.Nil(err, err)
		instance.SetLoggerCollectorConfig(&c)
		wg.Done()
		loggers.Info("logger collector viper配置完毕", ctx)

	}, func(v *viper.Viper) {
		ctx := context.Background()
		c := rabbit_zap.LoggerCollectorConfig{}
		err := v.Unmarshal(&c)
		asserts.Nil(err, err)
		instance.SetLoggerCollectorConfig(&c)
		loggers.Info("logger collector viper配置更新完毕", ctx)

	}, v.GetString(viperEtcdEndpointConfig), v.GetString(viperLogCollectorPathConfig))
	wg.Wait()
}
