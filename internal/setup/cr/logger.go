package cr

import (
	"context"
	"gitee.com/lailonghui/vehicle-supervision-framework/pkg/asserts"
	"gitee.com/lailonghui/vehicle-supervision-framework/pkg/loggers"
	"gitee.com/lailonghui/vehicle-supervision-framework/pkg/loggers/rabbit_zap"
	"gitee.com/shiqiyue/xd-bi/internal/pkg/instance"
	kitLog "github.com/go-kit/kit/log"
	kitZaplog "github.com/go-kit/kit/log/zap"
	"go.uber.org/zap"
)

// 日志初始化
func LoggerInit(ctx context.Context) {
	config := instance.GetConfig()
	loggers.SetAppName(instance.GetAppName())
	rabbitmqWriter, err := rabbit_zap.CreateByConfig(instance.GetLoggerCollectorConfig())
	asserts.Nil(err, err)
	appLogger := loggers.NewLogger("app", config.Log.File.Path, loggers.ParseLevel(config.Log.Level), loggers.ParseMode(config.Log.Mode), rabbitmqWriter)
	errorLogger := loggers.NewLogger("error", config.Log.File.Path, zap.ErrorLevel, loggers.ParseMode(config.Log.Mode), rabbitmqWriter)
	instance.SetAppLog(appLogger)
	loggers.RegisterSimpleLogger(appLogger)
	loggers.RegisterErrorLogger(errorLogger)
	var klog kitLog.Logger = kitZaplog.NewZapSugarLogger(appLogger, loggers.ParseLevel(config.Log.Level))
	instance.SetKitLog(klog)

}
