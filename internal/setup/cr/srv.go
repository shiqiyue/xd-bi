package cr

import (
	"context"
	"gitee.com/lailonghui/vehicle-supervision-framework/pkg/asserts"
	"gitee.com/lailonghui/vehicle-supervision-framework/pkg/beans"
	"gitee.com/lailonghui/vehicle-supervision-framework/pkg/loggers"
	"gitee.com/shiqiyue/xd-bi/internal/modules/cr/srv"
)

// 初始化Service
func ServiceInit(ctx context.Context) {
	srv.Reportinit()
	srv.ReportDbinit()
	srv.ReportForminit()
	srv.ReportResultColumninit()
	srv.SqlReportinit()
	srv.DictInit()
	srv.DictCategoryInit()
	srv.DictSqlInit()
	srv.ReportAggregateInit()
	reportExecSrv := srv.ReportExecInit()

	// 注入
	err := beans.Populate()
	asserts.Nil(err, err)

	reportExecSrv.StartScheduleCacheFlush()

	loggers.Info("service init done", nil)

}
