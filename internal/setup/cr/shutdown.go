package cr

import (
	"context"
	"gitee.com/lailonghui/vehicle-supervision-framework/pkg/shutdowns"
)

func ShutdownHandlerRegister(ctx context.Context, errc chan error) {
	go shutdowns.InterruptHandler(errc)
}
