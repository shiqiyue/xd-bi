package cr

import (
	"context"
	"gitee.com/lailonghui/vehicle-supervision-framework/pkg/loggers"
	"gitee.com/lailonghui/vehicle-supervision-framework/pkg/xids"
	"github.com/rs/xid"
	"go.uber.org/zap"
)

func XidInit() {
/*	config := instance.GetConfig()
	client := http.Client{}
	client.Timeout = time.Second*/
	xids.InitXid(func(ctx context.Context) string {
		/*var parentCtx opentracing.SpanContext
		if parent := opentracing.SpanFromContext(ctx); parent != nil {
			parentCtx = parent.Context()
		}
		sp := opentracing.StartSpan(
			"xid",
			opentracing.FollowsFrom(parentCtx),
			ext.SpanKindRPCClient,
		)
		defer sp.Finish()
		get, err := client.Get(config.Xid.Url)
		if err != nil {
			// 降级,使用本地xid生成
			loggers.Error("获取xid异常，降级", nil)
			return xid.New().String()
		}
		bs, err := ioutil.ReadAll(get.Body)
		if err != nil {
			// 降级,使用本地xid生成
			loggers.Error("获取xid异常，降级", nil)
			return xid.New().String()
		}
		return string(bs)*/
		return xid.New().String()
	})
	loggers.Info("xids测试", nil, zap.String("value", xids.GetXid(context.Background())))

}
