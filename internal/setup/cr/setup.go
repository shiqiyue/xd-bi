package cr

import (
	"context"
	"gitee.com/lailonghui/vehicle-supervision-framework/pkg/apms"
	"gitee.com/shiqiyue/xd-bi/internal/pkg/instance"
)

// 启动
func InitSetup(ctx context.Context) chan error {
	ConfigInit(ctx)

	LoggerInit(ctx)

	apms.InitOpenTracing("bi", instance.GetApmConfig().OpenTracing)

	XidInit()

	errc := make(chan error)

	ClientInit()

	DbInit(ctx)

	DaoInit(ctx)

	ServiceInit(ctx)

	// Interrupt handler.
	ShutdownHandlerRegister(ctx, errc)

	RestfulInit(ctx, errc)

	GqlgenInit(ctx, errc)

	GrpcInit(errc, registerGrpcServer)

	return errc
}
