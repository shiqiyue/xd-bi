package dao

import (
	"gitee.com/lailonghui/vehicle-supervision-framework/pkg/asserts"
	"gitee.com/lailonghui/vehicle-supervision-framework/pkg/beans"
	"gitee.com/shiqiyue/xd-bi/internal/modules/cr/model"
)

func SqlReportinit() {
	sqlReportDao := NewSqlReportDao()
	err := beans.ProvideBean(sqlReportDao)
	asserts.Nil(err, err)
}

type SqlReportDao struct {
	BaseDao
}

func NewSqlReportDao() *SqlReportDao {
	return &SqlReportDao{
		BaseDao: BaseDao{Model: model.SqlReport{}},
	}
}
