package dao

import (
	"context"
	"fmt"
	"gitee.com/shiqiyue/xd-bi/configs"
	"gitee.com/shiqiyue/xd-bi/internal/modules/cr/model"
	"github.com/dgryski/trifles/uuid"
	"github.com/stretchr/testify/assert"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"testing"
)

func initDb() (*gorm.DB, error) {
	dbNode := configs.DbNode{
		Host:     "192.168.3.132",
		Port:     5432,
		Username: "postgres",
		Password: "root",
		Dbname:   "common_report",
		SslMode:  "disable",
		Timezone: "Asia/Shanghai",
	}
	DB, err := gorm.Open(postgres.Open(dbNode.GetDsn()))
	return DB, err
}

func TestNewReportDao(t *testing.T) {
	dao := NewReportDao()
	db, err := initDb()
	assert.Nil(t, err)

	reportId := uuid.UUIDv4()

	ctx := context.Background()

	t.Run("插入测试", func(t *testing.T) {
		err = dao.Insert(ctx, db, &model.Report{

			ReportId: reportId,
			Type:     1,
			Status:   1,
			Code:     "dsad",
		})
		assert.Nil(t, err)
		report := &model.Report{}
		err = dao.GetByUnionId(ctx, db, reportId, report)
		assert.Nil(t, err)
		fmt.Println(report)
	})

	t.Run("更新测试", func(t *testing.T) {
		// 更新测试
		newCode := "c++"
		report := &model.Report{}
		err = dao.UpdateByUnionId(ctx, db, reportId, map[string]interface{}{
			"code": newCode,
		})
		assert.Nil(t, err)
		err = dao.GetByUnionId(ctx, db, reportId, report)
		assert.Nil(t, err)
		assert.Equal(t, report.Code, newCode)
	})

	t.Run("删除测试", func(t *testing.T) {
		report := &model.Report{}
		err = dao.RemoveByUnionId(ctx, db, reportId)
		assert.Nil(t, err)
		err = dao.GetByUnionId(ctx, db, reportId, report)
		assert.NotNil(t, err)
		assert.Equal(t, err.Error(), gorm.ErrRecordNotFound.Error())
	})

}
