package dao

import (
	"gitee.com/lailonghui/vehicle-supervision-framework/pkg/asserts"
	"gitee.com/lailonghui/vehicle-supervision-framework/pkg/beans"
	"gitee.com/shiqiyue/xd-bi/internal/modules/cr/model"
)

func ReportDbinit() {
	reportDbDao := NewReportDbDao()
	err := beans.ProvideBean(reportDbDao)
	asserts.Nil(err, err)
}

type ReportDbDao struct {
	*BaseDao
}

func NewReportDbDao() *ReportDbDao {
	return &ReportDbDao{
		BaseDao: &BaseDao{Model: model.ReportDb{}},
	}
}
