package dao

import (
	"gitee.com/lailonghui/vehicle-supervision-framework/pkg/asserts"
	"gitee.com/lailonghui/vehicle-supervision-framework/pkg/beans"
	"gitee.com/shiqiyue/xd-bi/internal/modules/cr/model"
)

func ReportAggregateInit() {
	dao := NewReportAggregateDao()
	err := beans.ProvideBean(dao)
	asserts.Nil(err, err)
}

type ReportAggregateDao struct {
	BaseDao
}

func NewReportAggregateDao() *ReportAggregateDao {
	return &ReportAggregateDao{
		BaseDao: BaseDao{Model: model.ReportAggregate{}},
	}
}
