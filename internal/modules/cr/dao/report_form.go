package dao

import (
	"gitee.com/lailonghui/vehicle-supervision-framework/pkg/asserts"
	"gitee.com/lailonghui/vehicle-supervision-framework/pkg/beans"
	"gitee.com/shiqiyue/xd-bi/internal/modules/cr/model"
)

func ReportForminit() {
	reportFormDao := NewReportFormDao()
	err := beans.ProvideBean(reportFormDao)
	asserts.Nil(err, err)
}

type ReportFormDao struct {
	BaseDao
}

func NewReportFormDao() *ReportFormDao {
	return &ReportFormDao{
		BaseDao: BaseDao{Model: model.ReportForm{}},
	}
}
