package dao

import (
	"gitee.com/lailonghui/vehicle-supervision-framework/pkg/asserts"
	"gitee.com/lailonghui/vehicle-supervision-framework/pkg/beans"
	"gitee.com/shiqiyue/xd-bi/internal/modules/cr/model"
)

func Reportinit() {
	reportDao := NewReportDao()
	err := beans.ProvideBean(reportDao)
	asserts.Nil(err, err)
}

type ReportDao struct {
	*BaseDao
}

func NewReportDao() *ReportDao {
	return &ReportDao{
		BaseDao: &BaseDao{Model: model.Report{}},
	}
}
