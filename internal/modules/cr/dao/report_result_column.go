package dao

import (
	"gitee.com/lailonghui/vehicle-supervision-framework/pkg/asserts"
	"gitee.com/lailonghui/vehicle-supervision-framework/pkg/beans"
	"gitee.com/shiqiyue/xd-bi/internal/modules/cr/model"
)

func ReportResultColumninit() {
	dao := NewReportResultColumnDao()
	err := beans.ProvideBean(dao)
	asserts.Nil(err, err)
}

type ReportResultColumnDao struct {
	BaseDao
}

func NewReportResultColumnDao() *ReportResultColumnDao {
	return &ReportResultColumnDao{
		BaseDao: BaseDao{Model: model.ReportResultColumn{}},
	}
}
