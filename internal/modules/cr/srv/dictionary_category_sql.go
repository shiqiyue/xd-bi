package srv

import "gitee.com/lailonghui/vehicle-supervision-framework/pkg/sqls"

var DictCategoryPageSql = sqls.RawSql{
	Id:  "DictCategoryPageSql",
	Sql: `
	select
	dictionary_category_id as DictionaryCategoryID,
	category_name as CategoryName,
	category_code as CategoryCode,
	remarks as Remarks,
	created_at as CreatedAt,
	created_by as CreatedBy,
	updated_at as UpdatedAt,
	updated_by as UpdatedBy,
	category_type as CategoryType
	from sys_dictionary_category
	where
	1=1
	{{if .CategoryName}}
	and category_name like concat('%', @CategoryName, '%')
	{{end}}
	{{if .CategoryCode}}
	and category_code like concat('%', @CategoryCode, '%')
	{{end}}
	order by
	{{if eq .OrderByColumn "category_name"}}
	category_name
	{{else if eq .OrderByColumn "category_code"}}
	category_code
	{{else if eq .OrderByColumn "category_type"}}
	category_type
	{{else if eq .OrderByColumn "created_at"}}
	created_at
	{{else if eq .OrderByColumn "updated_at"}}
	updated_at
	{{else}}
	id
	{{end}}
	{{if eq .OrderByDirection "desc"}}
	desc
	{{else}}
	asc
	{{end}}
	limit @limit offset @offset
`,
}

var DictCategoryCountSql = sqls.RawSql{
	Id:  "DictCategoryCountSql",
	Sql: `
	select
	count(*)
	from sys_dictionary_category
	where
	1=1
	{{if .CategoryName}}
	and category_name like concat('%', @CategoryName, '%')
	{{end}}
	{{if .CategoryCode}}
	and category_code like concat('%', @CategoryCode, '%')
	{{end}}

`,
}

