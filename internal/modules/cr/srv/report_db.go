package srv

import (
	"context"
	"gitee.com/lailonghui/vehicle-supervision-framework/pkg/asserts"
	"gitee.com/lailonghui/vehicle-supervision-framework/pkg/beans"
	"gitee.com/shiqiyue/xd-bi/internal/modules/cr/dao"
	"gorm.io/gorm"
)

func ReportDbinit() {
	sqlReportSrv := &ReportDbSrv{}
	err := beans.ProvideBean(sqlReportSrv)
	asserts.Nil(err, err)
}

type ReportDbSrv struct {
	BaseSrv
	*dao.ReportDbDao `inject:""`
	Db               *gorm.DB `inject:""`
}

func (s *ReportDbSrv) IsNameValid(ctx context.Context, db *gorm.DB, name string, reportDbId *string) (bool, error) {
	db = db.Model(s.Model.GetModel()).Where("name = ?", name)
	if reportDbId != nil {
		db = db.Where("report_db_id != ?", &reportDbId)
	}
	var num int64
	err := db.Count(&num).Error
	if err != nil {
		return false, err
	}
	if num > 0 {
		return false, nil
	}
	return true, nil
}
