package srv

import (
	"context"
	"gitee.com/lailonghui/vehicle-supervision-framework/pkg/asserts"
	"gitee.com/lailonghui/vehicle-supervision-framework/pkg/beans"
	"gitee.com/lailonghui/vehicle-supervision-framework/pkg/ferror"
	"gitee.com/lailonghui/vehicle-supervision-framework/pkg/gorms"
	"gitee.com/lailonghui/vehicle-supervision-framework/pkg/scalars"
	"gitee.com/shiqiyue/xd-bi/internal/modules/cr/model"
	"gorm.io/gorm"
)

func AreaInit() {
	srv := &AreaSrv{}
	err := beans.ProvideBean(srv)
	asserts.Nil(err, err)
}

type AreaSrv struct {
	Db *gorm.DB `inject:""`
}

func (s *AreaSrv) GetAreaListByPid(ctx context.Context, pid string) ([]*model.Area, error) {
	db := gorms.GetDb(ctx, s.Db)
	queryParam := &model.Area{}
	if pid == "" {
		queryParam.Pid = scalars.GetStringRef("-1")
	} else {
		queryParam.Pid = &pid
	}
	rs := make([]*model.Area, 0)
	err := db.Model(&model.Area{}).Where(queryParam).Find(&rs).Error
	if err != nil {
		return nil, ferror.Wrap("获取区域列表失败", err)
	}
	return rs, nil
}
