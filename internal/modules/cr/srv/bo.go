package srv

import "gitee.com/shiqiyue/xd-bi/internal/modules/cr/model"

// 报表Bo
type ReportBo struct {
	Report *model.Report

	SqlReport *model.SqlReport

	model.ReportForm
}

// 报表执行结果
type ReportExecResult struct {
	Count *int
	// 报表类型
	Type int

	List []map[string]interface{}

	Detail map[string]interface{}

	Sql []string

	CountSql []string

	Aggregate map[string]interface{}
}

// SQL报表执行结果
type SqlReportExecResult struct {
	Count *int
	// SQL展示报表类型
	DisplayType int

	List []map[string]interface{}

	Detail map[string]interface{}

	Sql string

	CountSql string
}

// 聚合执行结果
type AggregateExecuteResult struct {
	// 执行结果
	ExecResult SqlReportExecResult
	// 属性
	AttributeName string
	// 错误
	Error error
}
