package srv

import "gitee.com/lailonghui/vehicle-supervision-framework/pkg/sqls"

var ReportExecPageSql = sqls.RawSql{
	Id:  "ReportExecPageSql",
	Sql: `
select
exec_id as ExecId,
start_time as StartTime,
end_time as EndTime,
trace_id as TraceId,
report_id as ReportId,
params as Params,
use_time as UseTime,
success as Success,
error_msg as ErrorMsg,
user_id as UserId
from cr_report_exec
where
1=1
{{if .report_id}}
and report_id = @report_id
{{end}}
{{if .start_time}}
and start_time >= @start_time
{{end}}
{{if .end_time}}
and start_time <= @end_time
{{end}}
{{if .success}}
	{{if eq .success 1}}
	and success = true
	{{else}}
	and success = false
	{{end}}
{{end}}
{{if .use_time_min}}
and use_time >= @use_time_min
{{end}}
{{if .use_time_max}}
and use_time <= @use_time_max
{{end}}
{{if .user_id}}
and user_id = @user_id
{{end}}
order by
{{if eq .OrderByColumn "use_time"}}
use_time
{{else if eq .OrderByColumn "start_time"}}
start_time
{{else if eq .OrderByColumn "end_time"}}
end_time
{{else if eq .OrderByColumn "success"}}
success
{{else}}
id
{{end}}
{{if eq .OrderByDirection "desc"}}
desc
{{else}}
asc
{{end}}
limit @limit offset @offset
`,
}


var ReportExecCountSql = sqls.RawSql{
	Id:  "ReportExecCountSql",
	Sql: `
select
count(*)
where
1=1
{{if .report_id}}
and report_id = @report_id
{{end}}
{{if .start_time}}
and start_time >= @start_time
{{end}}
{{if .end_time}}
and start_time <= @end_time
{{end}}
{{if .success}}
	{{if eq .success 1}}
	and success = true
	{{else}}
	and success = false
	{{end}}
{{end}}
{{if .use_time_min}}
and use_time >= @use_time_min
{{end}}
{{if .use_time_max}}
and use_time <= @use_time_max
{{end}}
{{if .user_id}}
and user_id = @user_id
{{end}}

`,
}


var ReportDailySuccessRateSql = sqls.RawSql{
	Id:  "ReportDailySuccessRateSql",
	Sql: `
with success_count as (
select
from cr_report_exec
)

select

from
cr_report_exec
where
report_id = @report_id
and exec_time between @start_date and @end_date

`,
}
