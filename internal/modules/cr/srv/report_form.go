package srv

import (
	"context"
	"gitee.com/lailonghui/vehicle-supervision-framework/pkg/asserts"
	"gitee.com/lailonghui/vehicle-supervision-framework/pkg/beans"
	"gitee.com/shiqiyue/xd-bi/internal/modules/cr/dao"
	"gitee.com/shiqiyue/xd-bi/internal/modules/cr/enums"
	"gitee.com/shiqiyue/xd-bi/internal/modules/cr/model"
	"gorm.io/gorm"
)

func ReportForminit() {
	reportFormSrv := &ReportFormSrv{}
	err := beans.ProvideBean(reportFormSrv)
	asserts.Nil(err, err)
}

type ReportFormSrv struct {
	BaseSrv
	*dao.ReportFormDao `inject:""`
	Db                 *gorm.DB `inject:""`
}

// 通过报表ID删除
func (s *ReportFormSrv) RemoveByReportId(ctx context.Context, tx *gorm.DB, reportID string) error {
	err := tx.Model(s.Model.GetModel()).Where("report_id = ?", reportID).Delete(s.Model.GetModel()).Error
	return err
}

func (s *ReportFormSrv) FormDefaultValue(form model.ReportForm) interface{} {
	if form.Type == enums.REPORT_FORM_TYPE_TEXT {
		return ""
	}
	switch form.Type {
	case enums.REPORT_FORM_TYPE_TEXT:
		return ""
	case enums.REPORT_FORM_TYPE_NUMBER:
		return 1
	case enums.REPORT_FORM_TYPE_SELECT:
		return 1
	case enums.REPORT_FORM_TYPE_DATE:
		return "2020-02-02 22:22:22"

	}
	return nil
}

func (s *ReportFormSrv) getByReportIds(ctx context.Context, db *gorm.DB, reportIds []interface{}) ([]model.ReportForm, error) {
	result := make([]model.ReportForm, 0)
	err := db.Model(result).Where("report_id in ?", reportIds).Find(&result).Error
	if err != nil {
		return nil, err
	}
	return result, nil
}

// 通过reportId获取
func (d *ReportFormSrv) GetByReportId(ctx context.Context, db *gorm.DB, reportId string) ([]model.ReportForm, error) {
	result := make([]model.ReportForm, 0)
	err := db.Model(result).Where("report_id = ?", reportId).Find(&result).Error
	if err != nil {
		return nil, err
	}
	return result, nil
}
