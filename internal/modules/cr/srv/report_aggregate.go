package srv

import (
	"context"
	"gitee.com/lailonghui/vehicle-supervision-framework/pkg/asserts"
	"gitee.com/lailonghui/vehicle-supervision-framework/pkg/beans"
	"gitee.com/shiqiyue/xd-bi/internal/modules/cr/dao"
	"gitee.com/shiqiyue/xd-bi/internal/modules/cr/model"
	"gorm.io/gorm"
)

func ReportAggregateInit() {
	srv := &ReportAggregateSrv{}
	err := beans.ProvideBean(srv)
	asserts.Nil(err, err)
}

type ReportAggregateSrv struct {
	BaseSrv
	*dao.ReportAggregateDao `inject:""`
	Db                      *gorm.DB `inject:""`
}

func (s *ReportAggregateSrv) RemoveByReportId(ctx context.Context, tx *gorm.DB, reportID string) error {
	err := tx.Model(s.Model.GetModel()).Where("report_id = ?", reportID).Delete(s.Model.GetModel()).Error
	return err
}

func (s *ReportAggregateSrv) GetByReportId(ctx context.Context, db *gorm.DB, reportId string) ([]*model.ReportAggregate, error) {
	var result []*model.ReportAggregate
	err := db.Model(&model.ReportAggregate{}).Where("report_id = ?", reportId).Find(&result).Error
	return result, err
}
