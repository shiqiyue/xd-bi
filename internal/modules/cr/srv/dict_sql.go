package srv

import "gitee.com/lailonghui/vehicle-supervision-framework/pkg/sqls"

var DictPageSql = sqls.RawSql{
	Id:  "DictPageSql",
	Sql: `
	select
	dictionary_id as DictionaryID,
	dictionary_category_id as DictionaryCategoryID,
	name as Name,
	value as Value,
	remarks as Remarks,
	created_at as CreatedAt,
	created_by as CreatedBy,
	updated_at as UpdatedAt,
	updated_by as UpdatedBy,
	category_code as CategoryCode
	from sys_dictionary
	where
	1=1
	{{if .Name}}
	and name like concat('%', @Name, '%')
	{{end}}
	{{if .DictCategoryID}}
	and dictionary_category_id = @DictCategoryID
	{{end}}
	order by
	{{if eq .OrderByColumn "name"}}
	name
	{{else if eq .OrderByColumn "remarks"}}
	remarks
	{{else if eq .OrderByColumn "value"}}
	value
	{{else if eq .OrderByColumn "created_at"}}
	created_at
	{{else if eq .OrderByColumn "updated_at"}}
	updated_at
	{{else}}
	id
	{{end}}
	{{if eq .OrderByDirection "desc"}}
	desc
	{{else}}
	asc
	{{end}}
	limit @limit offset @offset
`,
}

var DictCountSql = sqls.RawSql{
	Id:  "DictCountSql",
	Sql: `
	select
	count(*)
	from sys_dictionary
	where
	1=1
	{{if .Name}}
	and name like concat('%', @Name, '%')
	{{end}}
	{{if .DictCategoryID}}
	and dictionary_category_id = @DictCategoryID
	{{end}}


`,
}

