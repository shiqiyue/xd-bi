package mgr

import (
	"gitee.com/shiqiyue/xd-bi/internal/modules/cr/srv"
	"gorm.io/gorm"
)

// This file will not be regenerated automatically.
//
// It serves as dependency injection for your app, add any dependencies you require here.

type Resolver struct {
	Db                    *gorm.DB                   `inject:""`
	ReportSrv             *srv.ReportSrv             `inject:""`
	ReportFormSrv         *srv.ReportFormSrv         `inject:""`
	SqlReportSrv          *srv.SqlReportSrv          `inject:""`
	ReportDbSrv           *srv.ReportDbSrv           `inject:""`
	ReportResultColumnSrv *srv.ReportResultColumnSrv `inject:""`
	ReportAggregateSrv    *srv.ReportAggregateSrv    `inject:""`
	ReportExecSrv         *srv.ReportExecSrv         `inject:""`
	DictSrv               *srv.DictSrv               `inject:""`
	DictCategorySrv       *srv.DictCategorySrv       `inject:""`
	DictSqlSrv            *srv.DictSqlSrv            `inject:""`
}
