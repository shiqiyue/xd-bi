package mgr

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"context"

	"gitee.com/lailonghui/vehicle-supervision-framework/pkg/ferror"
	"gitee.com/lailonghui/vehicle-supervision-framework/pkg/resultcode"
	"gitee.com/shiqiyue/xd-bi/internal/modules/cr/gqlgen/mgr/gmodel"
	"gitee.com/shiqiyue/xd-bi/internal/modules/cr/model"
)

func (r *queryResolver) ReportExecPage(ctx context.Context, req gmodel.ReportExecPageQuery) (*gmodel.ReportExecPageResult, error) {
	records, c, err := r.ReportExecSrv.Page(ctx, req)
	if err != nil {
		return nil, ferror.WrapCode(resultcode.FAIL, err)
	}
	return &gmodel.ReportExecPageResult{
		Total:   c,
		Records: records,
	}, nil
}

func (r *queryResolver) ReportDailySuccessRate(ctx context.Context, req gmodel.ReportStatisticsReq) ([]*gmodel.DailyRate, error) {
	return r.ReportExecSrv.ReportDailySuccessRate(ctx, req)
}

func (r *reportExecResolver) Report(ctx context.Context, obj *model.ReportExec) (*model.Report, error) {
	if obj == nil {
		return nil, nil
	}
	re := &model.Report{}
	err := r.ReportSrv.GetByUnionId(ctx, r.Db, obj.ReportId, re)
	if err != nil {
		return nil, err
	}
	return re, nil
}

// ReportExec returns ReportExecResolver implementation.
func (r *Resolver) ReportExec() ReportExecResolver { return &reportExecResolver{r} }

type reportExecResolver struct{ *Resolver }
