package mgr

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"context"
	"time"

	"gitee.com/lailonghui/vehicle-supervision-framework/pkg/auths"
	"gitee.com/lailonghui/vehicle-supervision-framework/pkg/ferror"
	"gitee.com/lailonghui/vehicle-supervision-framework/pkg/resultcode"
	"gitee.com/lailonghui/vehicle-supervision-framework/pkg/xids"
	"gitee.com/shiqiyue/xd-bi/internal/modules/cr/gqlgen/mgr/gmodel"
	model1 "gitee.com/shiqiyue/xd-bi/internal/modules/cr/model"
	"gitee.com/shiqiyue/xd-bi/pkg/dicts"
)

func (r *dictCategoryResolver) Dicts(ctx context.Context, obj *model1.DictCategory) ([]*model1.Dict, error) {
	if obj == nil || obj.DictionaryCategoryID == "" {
		return []*model1.Dict{}, nil
	}
	ds, err := r.DictSrv.DictListByCategoryId(ctx, obj.DictionaryCategoryID)
	if err != nil {
		return nil, ferror.WrapWithCode("调用数据层接口查询字典列表失败", resultcode.FAIL, err)
	}

	return ds, nil
}

func (r *mutationResolver) DictCategoryAdd(ctx context.Context, req gmodel.DictCategoryAddReq) (bool, error) {
	err := r.DictCategorySrv.AddCategory(ctx, req.CategoryCode, req.CategoryName, req.CategoryType, req.Remarks)
	if err != nil {
		return false, ferror.WrapCode(resultcode.FAIL, err)
	}
	return true, nil
}

func (r *mutationResolver) DictCategoryEdit(ctx context.Context, req gmodel.DictCategoryEditReq) (bool, error) {
	err := r.DictCategorySrv.EditCategory(ctx, req.DictionaryCategoryID, req.CategoryCode, req.CategoryName, req.Remarks)
	if err != nil {
		return false, ferror.WrapCode(resultcode.FAIL, err)
	}
	return true, nil
}

func (r *mutationResolver) DictCategoryDelete(ctx context.Context, dictCategoryID string) (bool, error) {
	err := r.DictCategorySrv.DeleteCategory(ctx, dictCategoryID)
	if err != nil {
		return false, ferror.WrapCode(resultcode.FAIL, err)
	}
	return true, nil
}

func (r *mutationResolver) DictAdd(ctx context.Context, req gmodel.DictAddReq) (bool, error) {
	err := r.DictSrv.AddDict(ctx, req.DictionaryCategoryID, req.Name, req.Value, req.Remarks)
	if err != nil {
		return false, ferror.WrapCode(resultcode.FAIL, err)
	}
	return true, nil
}

func (r *mutationResolver) DictEdit(ctx context.Context, req gmodel.DictEditReq) (bool, error) {
	err := r.DictSrv.EditDict(ctx, req.DictionaryID, req.Name, req.Value, req.Remarks)
	if err != nil {
		return false, ferror.WrapCode(resultcode.FAIL, err)
	}
	return true, nil
}

func (r *mutationResolver) DictDelete(ctx context.Context, dictID string) (bool, error) {
	err := r.DictSrv.DeleteDict(ctx, dictID)
	if err != nil {
		return false, ferror.WrapCode(resultcode.FAIL, err)
	}
	return true, nil
}

func (r *mutationResolver) UpdateDictCategorySQL(ctx context.Context, req gmodel.DictCategoryUpdateReq) (bool, error) {
	if req.DictionarySQLID == nil {
		// 添加
		currentUserId := auths.GetUserId(ctx)
		now := time.Now()
		err := r.DictSqlSrv.Add(ctx, &model1.DictionarySQL{
			DictionarySQLID:      xids.GetXid(ctx),
			AllSQL:               req.AllSQL,
			Remarks:              req.Remarks,
			CreatedAt:            now,
			CreatedBy:            currentUserId,
			UpdatedAt:            &now,
			UpdatedBy:            currentUserId,
			FilterSQL:            &req.FilterSQL,
			DictionaryCategoryId: req.DictionaryCategoryID,
			DbID:                 &req.DbID,
		})
		if err != nil {
			return false, ferror.WrapCode(resultcode.FAIL, err)
		}

	} else {
		// 修改
		currentUserId := auths.GetUserId(ctx)
		now := time.Now()
		dictSql, err := r.DictSqlSrv.GetByDictCategoryId(ctx, req.DictionaryCategoryID)
		if err != nil {
			return false, ferror.WrapCode(resultcode.FAIL, err)
		}
		if dictSql == nil {
			return false, ferror.NewWithCode(resultcode.FAIL, "记录不存在不存在")
		}
		dictSql.AllSQL = req.AllSQL
		dictSql.FilterSQL = &req.FilterSQL
		dictSql.DbID = &req.DbID
		dictSql.Remarks = req.Remarks
		dictSql.UpdatedAt = &now
		dictSql.UpdatedBy = currentUserId
		err = r.DictSqlSrv.Edit(ctx, dictSql)
		if err != nil {
			return false, ferror.WrapCode(resultcode.FAIL, err)
		}
	}

	return true, nil
}

func (r *queryResolver) DictCategoryPage(ctx context.Context, req gmodel.DictCategoryPageReq) (*gmodel.DictCategoryPage, error) {
	dictCategories, c, err := r.DictCategorySrv.Page(ctx, req)
	if err != nil {
		return nil, ferror.WrapCode(resultcode.FAIL, err)
	}
	return &gmodel.DictCategoryPage{
		TotalRecord: c,
		Records:     dictCategories,
	}, nil
}

func (r *queryResolver) DictCategoryList(ctx context.Context) ([]*model1.DictCategory, error) {
	return r.DictCategorySrv.ListAll(ctx)
}

func (r *queryResolver) DictCategoryDetail(ctx context.Context, dictCategoryID string) (*model1.DictCategory, error) {
	return r.DictCategorySrv.GetByCategoryId(ctx, dictCategoryID)
}

func (r *queryResolver) DictPage(ctx context.Context, req *gmodel.DictPageReq) (*gmodel.DictPage, error) {
	dicts, c, err := r.DictSrv.Page(ctx, *req)
	if err != nil {
		return nil, ferror.WrapCode(resultcode.FAIL, err)
	}
	return &gmodel.DictPage{
		TotalRecord: c,
		Records:     dicts,
	}, nil
}

func (r *queryResolver) DictList(ctx context.Context, dictCategoryID string) (*gmodel.DictList, error) {
	dicts, err := r.DictSrv.DictListByCategoryId(ctx, dictCategoryID)
	if err != nil {
		return nil, ferror.WrapCode(resultcode.FAIL, err)
	}
	return &gmodel.DictList{
		Records: dicts,
	}, nil
}

func (r *queryResolver) DictDetail(ctx context.Context, dictID string) (*model1.Dict, error) {
	dict, err := r.DictSrv.GetByDictionaryId(ctx, dictID)
	if err != nil {
		return nil, ferror.WrapCode(resultcode.FAIL, err)
	}

	return dict, nil
}

func (r *queryResolver) DictCategoryGoCode(ctx context.Context, dictCategoryID string) (string, error) {
	category, err := r.DictCategorySrv.GetByCategoryId(ctx, dictCategoryID)
	if err != nil {
		return "", err
	}

	ds, err := r.DictSrv.DictListByCategoryId(ctx, dictCategoryID)
	if err != nil {
		return "", err
	}

	goCode := dicts.ToGo(*category, ds)
	return goCode, nil
}

func (r *queryResolver) DictCategoryNodejsCode(ctx context.Context, dictCategoryID string) (string, error) {
	category, err := r.DictCategorySrv.GetByCategoryId(ctx, dictCategoryID)
	if err != nil {
		return "", err
	}
	ds, err := r.DictSrv.DictListByCategoryId(ctx, dictCategoryID)
	if err != nil {
		return "", err
	}
	nodeCode := dicts.ToNodejs(*category, ds)
	return nodeCode, nil
}

func (r *queryResolver) DictCategorySQL(ctx context.Context, dictCategoryID string) (*model1.DictionarySQL, error) {
	dictionarySQL, err := r.DictSqlSrv.GetByDictCategoryId(ctx, dictCategoryID)
	if err != nil {
		return nil, err
	}

	return dictionarySQL, nil
}

// DictCategory returns DictCategoryResolver implementation.
func (r *Resolver) DictCategory() DictCategoryResolver { return &dictCategoryResolver{r} }

type dictCategoryResolver struct{ *Resolver }
