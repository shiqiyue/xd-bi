package grpcsrv

import (
	"encoding/json"
	"gitee.com/lailonghui/vehicle-supervision-framework/pkg/ferror"
	"gitee.com/lailonghui/vehicle-supervision-framework/pkg/gorms"
	"gitee.com/lailonghui/vehicle-supervision-framework/pkg/resultcode"
	"gitee.com/lailonghui/vehicle-supervision-framework/pkg/scalars"
	"gitee.com/shiqiyue/bi-client"
	"gitee.com/shiqiyue/xd-bi/internal/modules/cr/model"
	"gitee.com/shiqiyue/xd-bi/internal/modules/cr/srv"
	"golang.org/x/net/context"
	"gorm.io/gorm"
)

type BiSrv struct {
	Db                    *gorm.DB                   `inject:""`
	ReportSrv             *srv.ReportSrv             `inject:""`
	ReportFormSrv         *srv.ReportFormSrv         `inject:""`
	SqlReportSrv          *srv.SqlReportSrv          `inject:""`
	ReportResultColumnSrv *srv.ReportResultColumnSrv `inject:""`
	ReportDbSrv           *srv.ReportDbSrv           `inject:""`
	DictSrv               *srv.DictSrv               `inject:""`
	AreaSrv               *srv.AreaSrv               `inject:""`
}

func (h BiSrv) ReportInfoByReportCode(c context.Context, req *bi.ReportInfoReq) (*bi.ReportInfoResult, error) {
	if req.GetReportNo() == "" {
		return &bi.ReportInfoResult{}, nil
	}
	db := gorms.GetDb(c, h.Db)
	report := model.Report{}
	err := h.ReportSrv.GetByCode(c, db, req.GetReportNo(), &report)
	if err != nil {
		return nil, ferror.Wrap("获取报表信息异常", err)
	}
	reportForms, err := h.ReportFormSrv.GetByReportId(c, db, report.ReportId)
	if err != nil {
		return nil, ferror.Wrap("获取报表表单信息异常", err)
	}
	reportResultColumns, err := h.ReportResultColumnSrv.GetByReportId(c, db, report.ReportId)
	if err != nil {
		return nil, ferror.Wrap("获取报表返回信息异常", err)
	}
	result := &bi.ReportInfoResult{}
	result.PageSupport = report.PageSupport
	result.DisplayType = int32(report.DisplayType)
	result.Forms = make([]*bi.GetReportResultForm, 0)
	result.Results = make([]*bi.GetReportResultColumn, 0)
	for _, reportForm := range reportForms {
		form := &bi.GetReportResultForm{
			FormName:      reportForm.Name,
			FormType:      int32(reportForm.Type),
			AttributeName: reportForm.AttributeName,
			ExtraConfig:   scalars.StringIfNilDefault(reportForm.ExtraConfig, ""),
			IndexNum:      int32(scalars.IntIfNilDefault(reportForm.IndexNum, 0)),
			DefaultValue:  scalars.StringIfNilDefault(reportForm.DefaultValue, ""),
			Required:      reportForm.Required,
		}
		if reportForm.DictCategoryId != nil && *reportForm.DictCategoryId != "" && reportForm.ReturnAllDict {
			dicts, err := h.DictSrv.DictListByCategoryId(c, *reportForm.DictCategoryId)
			if err != nil {
				return nil, ferror.Wrap("获取字典列表失败", err)
			}
			ds := []*bi.DictInfo{}
			for _, dict := range dicts {
				ds = append(ds, &bi.DictInfo{
					Text:  scalars.StringIfNilDefault(dict.Remarks, ""),
					Value: int32(dict.Value),
				})
			}
			form.Dicts = ds
		}
		result.Forms = append(result.Forms, form)
	}
	for _, resultColumn := range reportResultColumns {
		column := &bi.GetReportResultColumn{
			ResultName:          resultColumn.Name,
			ResultType:          int32(resultColumn.Type),
			AttributeName:       resultColumn.AttributeName,
			IsShow:              resultColumn.IsShow,
			ExtraConfig:         scalars.StringIfNilDefault(resultColumn.ExtraConfig, ""),
			IndexNum:            int32(scalars.IntIfNilDefault(resultColumn.IndexNum, 0)),
			EmptyValueFontColor: scalars.StringIfNilDefault(resultColumn.EmptyValueFontColor, ""),
			FontColor:           scalars.StringIfNilDefault(resultColumn.FontColor, ""),
			IsJump:              resultColumn.IsJump,
			JumpType:            int32(scalars.IntIfNilDefault(resultColumn.JumpType, 0)),
			JumpTarget:          scalars.StringIfNilDefault(resultColumn.JumpTarget, ""),
			JumpParams:          make([]*bi.GetReportResultColumnJumpParam, 0),
		}
		if resultColumn.JumpParams != nil {
			jumpParams := make([]*model.ReportResultColumnJumpParam, 0)
			bytes, err := resultColumn.JumpParams.MarshalJSON()
			if err != nil {
				return nil, ferror.Wrap(resultcode.FAIL, err)
			}
			err = json.Unmarshal(bytes, &jumpParams)
			if err != nil {
				return nil, ferror.Wrap(resultcode.FAIL, err)
			}
			for _, param := range jumpParams {
				column.JumpParams = append(column.JumpParams, &bi.GetReportResultColumnJumpParam{
					ParamType: int32(param.ParamType),
					Name:      param.Name,
					Value:     param.Value,
				})
			}
		}
		result.Results = append(result.Results, column)
	}
	return result, nil

}
