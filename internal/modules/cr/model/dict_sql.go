package model

import "time"

// sql字典
type DictionarySQL struct {
	// id
	ID int64 `json:"id"`
	// sql字典ID
	DictionarySQLID string `json:"dictionary_sql_id"`
	// 字典ID
	DictionaryCategoryId string `json:"dictionary_category_id"`
	// 获取所有数据的sql脚本
	AllSQL string `json:"all_sql"`
	// 备注
	Remarks *string `json:"remarks"`
	// 创建时间
	CreatedAt time.Time `json:"created_at"`
	// 创建人
	CreatedBy *string `json:"created_by"`
	// 修改时间
	UpdatedAt *time.Time `json:"updated_at"`
	// 修改人
	UpdatedBy *string `json:"updated_by"`

	// 通过字典编号进行过滤的sql脚本
	FilterSQL *string `json:"filter_sql"`
	// 数据库ID
	DbID *string
	// 是否支持数据权限
	DataAccessSupport bool `json:"data_access_support"`
}

func (r DictionarySQL) TableName() string {
	return "sys_dictionary_sql"
}
