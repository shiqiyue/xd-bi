package model

import (
	"gorm.io/gorm"
	"time"
)

// sql报表
type SqlReport struct {
	ID        uint           `gorm:"primaryKey;default:id_generator();type:int8"`
	CreatedAt time.Time      `gorm:"not null"`
	UpdatedAt time.Time      `gorm:"not null"`
	DeletedAt gorm.DeletedAt `gorm:"index"`
	// sql报表ID
	SqlReportId string `gorm:"type:text;index;not null"`
	// 报表ID
	ReportId string `gorm:"type:text;index;not null"`
	// sql
	Sql string `gorm:"type:text;not null"`
	// 数据库ID
	DbId string `gorm:"type:text;not null"`
	// 是否支持go 模板
	SupportGoTemplate bool
	// count语句
	CountSql string `gorm:"type:text"`
}

func (s SqlReport) GetModel() interface{} {
	return &SqlReport{}
}

func (s SqlReport) GetTableName() string {
	return s.TableName()
}

func (s SqlReport) GetIdColumn() string {
	return "id"
}

func (s SqlReport) GetUnionIdColumn() string {
	return "sql_report_id"
}

func (s SqlReport) TableName() string {
	return "cr_sql_report"
}
