package model

import (
	"gorm.io/datatypes"
	"gorm.io/gorm"
	"time"
)

// 报表-返回结果列
type ReportResultColumn struct {
	ID        uint           `gorm:"primaryKey;default:id_generator();type:int8"`
	CreatedAt time.Time      `gorm:"not null"`
	UpdatedAt time.Time      `gorm:"not null"`
	DeletedAt gorm.DeletedAt `gorm:"index"`
	// 表单ID
	ReportResultColumnId string `gorm:"type:text;index;not null;foreignKey"`
	// 报表ID
	ReportId string `gorm:"type:text;index;not null"`
	// 返回结果类型
	Type int `gorm:"not null"`
	// 名称
	Name string `gorm:"not null"`
	// 属性名称
	AttributeName string `gorm:"not null"`
	// 字典分类ID
	DictCategoryId *string
	// 额外配置信息
	ExtraConfig *string
	// 是否展示
	IsShow bool `gorm:"not null"`
	// 顺序
	IndexNum *int
	// 空值颜色颜色
	EmptyValueFontColor *string
	// 空值
	EmptyValue *string
	// 字体颜色
	FontColor *string

	// 是否跳转
	IsJump bool `gorm:"not null; default: false"`
	// 跳转类型
	JumpType *int
	// 跳转目标
	JumpTarget *string

	JumpParams *datatypes.JSON `gorm:"type:jsonb"`

	// 跳转方式
	JumpMode *int
}

func (r ReportResultColumn) GetModel() interface{} {
	return &ReportResultColumn{}
}

func (r ReportResultColumn) GetTableName() string {
	return r.TableName()
}

func (r ReportResultColumn) GetIdColumn() string {
	return "id"
}

func (r ReportResultColumn) GetUnionIdColumn() string {
	return "report_result_column_id"
}

func (r ReportResultColumn) TableName() string {
	return "cr_report_result_column"
}
