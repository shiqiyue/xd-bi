package model

import (
	"gorm.io/gorm"
	"time"
)

// 报表聚合
type ReportAggregate struct {
	ID                uint           `gorm:"primaryKey;default:id_generator();type:int8"`
	CreatedAt         time.Time      `gorm:"not null"`
	UpdatedAt         time.Time      `gorm:"not null"`
	DeletedAt         gorm.DeletedAt `gorm:"index"`
	ReportAggregateId string         `gorm:"type:text;index;not null"`
	// 报表ID
	ReportId string `gorm:"type:text;index;not null"`
	// 关联报表ID
	ConnectReportId string
	// 属性名称
	AttributeName string
}

func (r ReportAggregate) GetModel() interface{} {
	return &ReportAggregate{}
}

func (r ReportAggregate) GetTableName() string {
	return r.TableName()
}

func (r ReportAggregate) GetIdColumn() string {
	return "id"
}

func (r ReportAggregate) GetUnionIdColumn() string {
	return "report_aggregate_id"
}

func (r ReportAggregate) TableName() string {
	return "cr_report_aggregate"
}
