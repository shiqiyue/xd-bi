package model

import "time"

type ReportExec struct {
	ID        int64     `json:"id"`
	ExecId    string    `json:"exec_id"`
	StartTime time.Time `json:"start_time"`
	EndTime   time.Time `json:"end_time"`
	TraceId   string    `json:"trace_id"`
	ReportId  string    `json:"report_id"`
	Params    string    `json:"params"`
	UseTime   int64     `json:"use_time"`
	Success   bool      `json:"success"`
	ErrorMsg  string    `json:"error_msg"`
	UserId    string    `json:"user_id"`
	ExecDate  time.Time `json:"exec_date"`
}

func (r ReportExec) TableName() string {
	return "cr_report_exec"
}
