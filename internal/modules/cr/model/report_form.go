package model

import (
	"gorm.io/gorm"
	"time"
)

// 报表-表单配置
type ReportForm struct {
	ID        uint           `gorm:"primaryKey;default:id_generator();type:int8"`
	CreatedAt time.Time      `gorm:"not null"`
	UpdatedAt time.Time      `gorm:"not null"`
	DeletedAt gorm.DeletedAt `gorm:"index"`
	// 表单ID
	ReportFormId string `gorm:"type:text;index;not null"`
	// 报表ID
	ReportId string `gorm:"type:text;index;not null"`
	// 表单类型
	Type int `gorm:"not null"`
	// 表单名称
	Name string `gorm:"not null"`
	// 属性名称
	AttributeName string `gorm:"not null"`
	// 字典分类ID
	DictCategoryId *string `gorm:"not null"`
	// 是否返回所有字典信息
	ReturnAllDict bool `gorm:"not null"`
	// 额外配置信息
	ExtraConfig *string
	// 顺序
	IndexNum *int
	// 默认值
	DefaultValue *string
	// 查询报表编号
	QueryReportCode *string

	// 是否必填
	Required bool `gorm:"not null; default:false"`
}

func (r ReportForm) GetModel() interface{} {
	return &ReportForm{}
}

func (r ReportForm) GetTableName() string {
	return r.TableName()
}

func (r ReportForm) GetIdColumn() string {
	return "id"
}

func (r ReportForm) GetUnionIdColumn() string {
	return "report_form_id"
}

func (r ReportForm) TableName() string {
	return "cr_report_form"
}
