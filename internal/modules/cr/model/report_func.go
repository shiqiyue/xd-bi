package model

import (
	"gorm.io/gorm"
	"time"
)

// 报表功能
type ReportFunc struct {
	ID        uint           `gorm:"primaryKey;default:id_generator();type:int8"`
	CreatedAt time.Time      `gorm:"not null"`
	UpdatedAt time.Time      `gorm:"not null"`
	DeletedAt gorm.DeletedAt `gorm:"index"`
	// 表单功能ID
	ReportFuncId string `gorm:"type:text;index;not null"`
	// 报表ID
	ReportId string `gorm:"type:text;index;not null"`
	// 功能类型
	FuncType int `gorm:"not null"`
	// 功能位于哪个字段
	ResultColumnId *string
	// 关联报表ID
	ConnReportId *string
}

func (r ReportFunc) GetModel() interface{} {
	return &ReportFunc{}
}

func (r ReportFunc) GetTableName() string {
	return r.TableName()
}

func (r ReportFunc) GetIdColumn() string {
	return "id"
}

func (r ReportFunc) GetUnionIdColumn() string {
	return "report_func_id"
}

func (r ReportFunc) TableName() string {
	return "cr_report_func"
}
