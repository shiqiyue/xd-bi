package model

import (
	"gorm.io/gorm"
	"time"
)

// 报表
type Report struct {
	ID        uint           `gorm:"primaryKey;default:id_generator();type:int8"`
	CreatedAt time.Time      `gorm:"not null"`
	UpdatedAt time.Time      `gorm:"not null"`
	DeletedAt gorm.DeletedAt `gorm:"index"`
	// 名称
	Name string `gorm:"not null"`
	// 报表ID
	ReportId string `gorm:"type:text;index;not null"`
	// 报表类型
	Type int `gorm:"not null"`
	// 状态
	Status int `gorm:"not null"`
	// 编号
	Code string `gorm:"not null"`
	// 展示类型
	DisplayType int `gorm:"not null"`
	// 是否支持数据权限
	DataAccessSupport bool `gorm:"not null"`
	// 是否支持分页
	PageSupport bool `gorm:"not null"`
	// 是否支持excel导出
	ExcelExportSupport bool `gorm:"not null"`
	// 是否显示合计
	ShowSummary bool `gorm:"not null;default:false;"`
}

func (r Report) GetModel() interface{} {
	return &Report{}
}

func (r Report) GetTableName() string {
	return r.TableName()
}

func (r Report) GetIdColumn() string {
	return "id"
}

func (r Report) GetUnionIdColumn() string {
	return "report_id"
}

func (r Report) TableName() string {
	return "cr_report"
}
