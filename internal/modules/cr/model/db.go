package model

import (
	"gorm.io/gorm"
	"time"
)

type ReportDb struct {
	ID         uint           `gorm:"primaryKey;default:id_generator();type:int8"`
	CreatedAt  time.Time      `gorm:"not null"`
	UpdatedAt  time.Time      `gorm:"not null"`
	DeletedAt  gorm.DeletedAt `gorm:"index"`
	ReportDbId string         `gorm:"not null"`
	// 数据库名称
	Name string `gorm:"not null"`
	// 数据库host
	Host string `gorm:"not null"`
	// 数据库端口
	Port int `gorm:"not null"`
	// 用户名
	Username string `gorm:"not null"`
	// 密码
	Password string `gorm:"not null"`
	// 数据库名称
	DbName string `gorm:"not null"`
	// 数据库类型
	Type int `gorm:"not null"`
	// 连接池最多空闲链接
	MaxIdleConns int `gorm:"not null"`
	// 连接池最多链接
	MaxOpenConns int `gorm:"not null"`
	//连接可复用的最大时间(单位，秒)
	ConnMaxLifetime int `gorm:"not null"`
}

func (r ReportDb) GetModel() interface{} {
	return &ReportDb{}
}

func (r ReportDb) GetTableName() string {
	return r.TableName()
}

func (r ReportDb) GetIdColumn() string {
	return "id"
}

func (r ReportDb) GetUnionIdColumn() string {
	return "report_db_id"
}

func (r ReportDb) TableName() string {
	return "cr_report_db"
}
