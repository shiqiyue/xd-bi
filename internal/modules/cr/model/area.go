package model

type Area struct {
	ID       int64   `json:"id"`
	AreaID   string  `json:"area_id"`
	Name     *string `json:"name"`
	Code     *string `json:"code"`
	Pid      *string `json:"pid"`
	Level    *int    `json:"level"`
	MapLevel *int    `json:"map_level"`
	AreaCode *string `json:"area_code"`
}

func (r Area) TableName() string {
	return "sys_area"
}
