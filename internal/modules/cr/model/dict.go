package model

import "time"

type Dict struct {
	ID                   int64      `json:"id"`
	DictionaryID         string     `json:"dictionary_id"`
	DictionaryCategoryID string     `json:"dictionary_category_id"`
	CategoryCode         *string    `json:"category_code"`
	Name                 string     `json:"name"`
	Value                int        `json:"value"`
	Remarks              *string    `json:"remarks"`
	CreatedAt            time.Time  `json:"created_at"`
	CreatedBy            *string    `json:"created_by"`
	UpdatedAt            *time.Time `json:"updated_at"`
	UpdatedBy            *string    `json:"updated_by"`
}

func (r Dict) TableName() string {
	return "sys_dictionary"
}
