package model

// 跳转参数
type ReportResultColumnJumpParam struct {

	// 返回列ID
	ReportResultColumnId uint `gorm:"not null"`
	// 参数类型
	ParamType int `gorm:"not null"`
	// 参数名称
	Name string `gorm:"not null"`
	// 值
	Value string `gorm:"not null"`
}
