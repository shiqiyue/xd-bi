package model

import "time"

type DictCategory struct {
	ID                   int64      `json:"id"`
	DictionaryCategoryID string    `json:"dictionary_category_id"`
	CategoryName         string    `json:"category_name"`
	CategoryCode         string    `json:"category_code"`
	Remarks              *string   `json:"remarks"`
	CreatedAt            time.Time `json:"created_at"`
	CreatedBy            *string   `json:"created_by"`
	UpdatedAt            time.Time `json:"updated_at"`
	UpdatedBy            *string   `json:"updated_by"`
	CategoryType         *int      `json:"CategoryType"`
}

func (r DictCategory) TableName() string {
	return "sys_dictionary_category"
}
