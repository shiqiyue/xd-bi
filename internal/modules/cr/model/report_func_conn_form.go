package model

import (
	"gorm.io/gorm"
	"time"
)

// 报表功能-关联报表-表单映射
type ReportFuncConnForm struct {
	ID        uint           `gorm:"primaryKey;default:id_generator();type:int8"`
	CreatedAt time.Time      `gorm:"not null"`
	UpdatedAt time.Time      `gorm:"not null"`
	DeletedAt gorm.DeletedAt `gorm:"index"`
	// 联合主键
	ReportFuncConnFormID string
	// 表单功能ID
	ReportFuncId string `gorm:"type:text;index;not null"`
	// 报表表单ID
	ReportFormId string `gorm:"type:text;index;not null"`
	// 值类型
	ValueType int `gorm:"not null"`
	// 固定值
	FixValue *string
	// 关联返回值ID
	ConnResultColumnId *string
}

func (r ReportFuncConnForm) GetModel() interface{} {
	return &ReportFunc{}
}

func (r ReportFuncConnForm) GetTableName() string {
	return r.TableName()
}

func (r ReportFuncConnForm) GetIdColumn() string {
	return "id"
}

func (r ReportFuncConnForm) GetUnionIdColumn() string {
	return "report_func_id"
}

func (r ReportFuncConnForm) TableName() string {
	return "cr_report_func"
}
