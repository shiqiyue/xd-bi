package restful

import (
	"gitee.com/lailonghui/vehicle-supervision-framework/pkg/ferror"
	"gitee.com/lailonghui/vehicle-supervision-framework/pkg/gins"
	"gitee.com/lailonghui/vehicle-supervision-framework/pkg/https"
	"gitee.com/lailonghui/vehicle-supervision-framework/pkg/resultcode"
	"github.com/gin-gonic/gin"
)

type SearchDictsReq struct {
	CategoryCode string `json:"category_codes" `
}

type SearchDictResult struct {
	Categorys map[string][]DictInfo
}

func (h *Handlers) SearchDicts(c *gin.Context) {
	queryParam := GetDictsReq{}
	if err := c.BindJSON(&queryParam); err != nil {
		gins.Err(c, ferror.WrapWithCode("解析参数失败", resultcode.FAIL, err), https.StatusInternalServerError)
		return
	}
	if queryParam.CategoryCodes == nil || len(queryParam.CategoryCodes) == 0 {
		gins.Err(c, ferror.NewWithCode("参数为空", resultcode.PARAM_REQUIRED), https.StatusInternalServerError)
		return
	}
	result := GetDictResult{}
	cs := map[string][]DictInfo{}
	for _, code := range queryParam.CategoryCodes {
		dicts, err := h.DictSrv.DictListByCategoryCode(c, code)
		if err != nil {
			gins.Err(c, ferror.WrapWithCode("获取字典列表失败", resultcode.FAIL, err), https.StatusInternalServerError)
			return
		}
		if dicts != nil {
			ds := []DictInfo{}
			for k, v := range dicts {
				ds = append(ds, DictInfo{
					Text:  v,
					Value: k,
				})
			}
			cs[code] = ds
		}
	}
	result.Categorys = cs

	gins.Suc(c, result)
}
