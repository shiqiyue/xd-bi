package restful

import (
	"gitee.com/lailonghui/vehicle-supervision-framework/pkg/ferror"
	"gitee.com/lailonghui/vehicle-supervision-framework/pkg/gins"
	"gitee.com/lailonghui/vehicle-supervision-framework/pkg/https"
	"gitee.com/lailonghui/vehicle-supervision-framework/pkg/resultcode"
	"gitee.com/shiqiyue/xd-bi/internal/modules/cr/model"
	"github.com/gin-gonic/gin"
)

type GetAreaResult struct {
	Areas []*model.Area `json:"areas"`
}

/**
@api {get} /bi/dict/area 获取区域信息
@apiSampleRequest http://192.168.3.132:9102/
@apiName 根据编号获取字典
@apiGroup 字典

@apiParam {String[]} category_codes 字典类型编号数组

@apiSuccess {String} msg 返回消息
@apiSuccess {String} code 返回编号
@apiSuccess {Object} data 返回数据

@apiSuccess {Object} data.Categorys 字典类型对象


@apiParamExample {json} 请求例子:
{
    "category_codes": ["common_http_method", "sys_api_permission_type"]
}

@apiSuccessExample 成功返回:
HTTP/1.1 200 OK
{
    "Categorys": {
        "common_http_method": [
            {
                "text": "GET",
                "value": 1
            },
            {
                "text": "POST",
                "value": 2
            },
            {
                "text": "PUT",
                "value": 3
            },
            {
                "text": "DELETE",
                "value": 4
            }
        ],
        "sys_api_permission_type": [
            {
                "text": "HTTP",
                "value": 1
            },
            {
                "text": "GQL",
                "value": 2
            }
        ]
    }
}
*/
// 获取区域
func (h *Handlers) GetArea(c *gin.Context) {
	pid := c.Query("pid")
	areaList, err := h.AreaSrv.GetAreaListByPid(c, pid)
	if err != nil {
		gins.Err(c, ferror.WrapWithCode("获取区域列表错误", resultcode.FAIL, err), https.StatusInternalServerError)
		return
	}
	result := GetAreaResult{Areas: areaList}
	gins.Suc(c, result)
}
