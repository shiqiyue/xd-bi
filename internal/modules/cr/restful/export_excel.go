package restful

import (
	"gitee.com/lailonghui/vehicle-supervision-framework/pkg/ferror"
	"gitee.com/lailonghui/vehicle-supervision-framework/pkg/gins"
	"gitee.com/lailonghui/vehicle-supervision-framework/pkg/gorms"
	"gitee.com/lailonghui/vehicle-supervision-framework/pkg/https"
	"gitee.com/lailonghui/vehicle-supervision-framework/pkg/resultcode"
	"github.com/gin-gonic/gin"
)

func (h Handlers) ExportExcel(c *gin.Context) {
	code := c.Param("code")
	db := gorms.GetDb(c.Request.Context(), h.Db)

	queryParam := map[string]interface{}{}

	if err := c.BindJSON(&queryParam); err != nil {
		gins.Err(c, ferror.WrapWithCode("解析参数失败", resultcode.FAIL, err), https.StatusInternalServerError)
		return
	}
	executeReport, err := h.ReportSrv.ExecuteReport(c.Request.Context(), db, code, queryParam)
	if err != nil {
		gins.Err(c, ferror.WrapWithCode("执行报表失败", resultcode.FAIL, err), https.StatusInternalServerError)
		return
	}

	gins.Suc(c, ExecReportResult{
		Count:     executeReport.Count,
		List:      executeReport.List,
		Detail:    executeReport.Detail,
		Aggregate: executeReport.Aggregate,
	})
}
