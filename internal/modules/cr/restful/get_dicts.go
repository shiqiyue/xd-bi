package restful

import (
	"gitee.com/lailonghui/vehicle-supervision-framework/pkg/ferror"
	"gitee.com/lailonghui/vehicle-supervision-framework/pkg/gins"
	"gitee.com/lailonghui/vehicle-supervision-framework/pkg/https"
	"gitee.com/lailonghui/vehicle-supervision-framework/pkg/resultcode"
	"github.com/gin-gonic/gin"
)

type GetDictsReq struct {
	CategoryCodes []string `json:"category_codes"`
}

type GetDictResult struct {
	Categorys map[string][]DictInfo
}

/**
@api {post} /bi/dict/listByCode 根据编号获取字典
@apiSampleRequest http://192.168.3.132:9102/
@apiName 根据编号获取字典
@apiGroup 字典

@apiParam {String[]} category_codes 字典类型编号数组

@apiSuccess {String} msg 返回消息
@apiSuccess {String} code 返回编号
@apiSuccess {Object} data 返回数据

@apiSuccess {Object} data.Categorys 字典类型对象


@apiParamExample {json} 请求例子:
{
    "category_codes": ["common_http_method", "sys_api_permission_type"]
}

@apiSuccessExample 成功返回:
HTTP/1.1 200 OK
{
    "Categorys": {
        "common_http_method": [
            {
                "text": "GET",
                "value": 1
            },
            {
                "text": "POST",
                "value": 2
            },
            {
                "text": "PUT",
                "value": 3
            },
            {
                "text": "DELETE",
                "value": 4
            }
        ],
        "sys_api_permission_type": [
            {
                "text": "HTTP",
                "value": 1
            },
            {
                "text": "GQL",
                "value": 2
            }
        ]
    }
}
*/
func (h *Handlers) GetDicts(c *gin.Context) {
	queryParam := GetDictsReq{}
	if err := c.BindJSON(&queryParam); err != nil {
		gins.Err(c, ferror.WrapWithCode("解析参数失败", resultcode.FAIL, err), https.StatusInternalServerError)
		return
	}
	if queryParam.CategoryCodes == nil || len(queryParam.CategoryCodes) == 0 {
		gins.Err(c, ferror.NewWithCode("参数为空", resultcode.PARAM_REQUIRED), https.StatusInternalServerError)
		return
	}
	result := GetDictResult{}
	cs := map[string][]DictInfo{}
	for _, code := range queryParam.CategoryCodes {
		dicts, err := h.DictSrv.DictListByCategoryCode(c, code)
		if err != nil {
			gins.Err(c, ferror.WrapWithCode("根据编号获取字典列表失败", resultcode.FAIL, err), https.StatusInternalServerError)
			return
		}
		if dicts != nil {
			ds := []DictInfo{}
			for k, v := range dicts {
				ds = append(ds, DictInfo{
					Text:  v,
					Value: k,
				})
			}
			cs[code] = ds
		}
	}
	result.Categorys = cs

	gins.Suc(c, result)
}
