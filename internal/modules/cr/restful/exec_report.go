package restful

import (
	"gitee.com/lailonghui/vehicle-supervision-framework/pkg/ferror"
	"gitee.com/lailonghui/vehicle-supervision-framework/pkg/gins"
	"gitee.com/lailonghui/vehicle-supervision-framework/pkg/gorms"
	"gitee.com/lailonghui/vehicle-supervision-framework/pkg/https"
	"gitee.com/lailonghui/vehicle-supervision-framework/pkg/resultcode"
	"github.com/gin-gonic/gin"
)

/**
@api {post} /bi/public/:code 执行报表
@apiSampleRequest http://192.168.3.132:9102/
@apiName 执行报表
@apiGroup 报表

@apiParam {String} code 报表编号

@apiSuccess {String} msg 返回消息
@apiSuccess {String} code 返回编号
@apiSuccess {Object} data 返回数据

@apiSuccess {Object[]} data.List 列表
@apiSuccess {Number} data.Count 数量
@apiSuccess {Object} data.Detail 详情

@apiSuccessExample Success-Response:
HTTP/1.1 200 OK
{
    "msg": "请求成功",
    "code": 200,
    "data": {
        "Count": 1,
        "List": [
            {
                "name": null,
                "organization_id": "testOrganization"
            }
        ]
    }
}
*/

type ExecReportResult struct {
	Count *int

	List []map[string]interface{}
	// 详情
	Detail map[string]interface{}

	Aggregate map[string]interface{}
}

func (h Handlers) ExecReport(c *gin.Context) {
	code := c.Param("code")
	db := gorms.GetDb(c.Request.Context(), h.Db)

	queryParam := map[string]interface{}{}

	if err := c.BindJSON(&queryParam); err != nil {
		gins.Err(c, ferror.WrapWithCode("解析参数失败", resultcode.FAIL, err), https.StatusInternalServerError)
		return
	}
	executeReport, err := h.ReportSrv.ExecuteReport(c.Request.Context(), db, code, queryParam)
	if err != nil {
		gins.Err(c, ferror.WrapWithCode("执行报表失败", resultcode.FAIL, err), https.StatusInternalServerError)
		return
	}

	gins.Suc(c, ExecReportResult{
		Count:     executeReport.Count,
		List:      executeReport.List,
		Detail:    executeReport.Detail,
		Aggregate: executeReport.Aggregate,
	})
}
