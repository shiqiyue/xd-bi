define({ "api": [
  {
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "optional": false,
            "field": "varname1",
            "description": "<p>No type.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "varname2",
            "description": "<p>With type.</p>"
          }
        ]
      }
    },
    "type": "",
    "url": "",
    "version": "0.0.0",
    "filename": "./doc/main.js",
    "group": "D:\\project\\bi\\internal\\modules\\cr\\restful\\doc\\main.js",
    "groupTitle": "D:\\project\\bi\\internal\\modules\\cr\\restful\\doc\\main.js",
    "name": ""
  },
  {
    "type": "get",
    "url": "/bi/dict/area",
    "title": "获取区域信息",
    "sampleRequest": [
      {
        "url": "http://192.168.3.132:9102/"
      }
    ],
    "name": "根据编号获取字典",
    "group": "字典",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String[]",
            "optional": false,
            "field": "category_codes",
            "description": "<p>字典类型编号数组</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "请求例子:",
          "content": "{\n    \"category_codes\": [\"common_http_method\", \"sys_api_permission_type\"]\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>返回消息</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "code",
            "description": "<p>返回编号</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>返回数据</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data.Categorys",
            "description": "<p>字典类型对象</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "成功返回:",
          "content": "HTTP/1.1 200 OK\n{\n    \"Categorys\": {\n        \"common_http_method\": [\n            {\n                \"text\": \"GET\",\n                \"value\": 1\n            },\n            {\n                \"text\": \"POST\",\n                \"value\": 2\n            },\n            {\n                \"text\": \"PUT\",\n                \"value\": 3\n            },\n            {\n                \"text\": \"DELETE\",\n                \"value\": 4\n            }\n        ],\n        \"sys_api_permission_type\": [\n            {\n                \"text\": \"HTTP\",\n                \"value\": 1\n            },\n            {\n                \"text\": \"GQL\",\n                \"value\": 2\n            }\n        ]\n    }\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./get_area.go",
    "groupTitle": "字典"
  },
  {
    "type": "post",
    "url": "/bi/dict/listByCode",
    "title": "根据编号获取字典",
    "sampleRequest": [
      {
        "url": "http://192.168.3.132:9102/"
      }
    ],
    "name": "根据编号获取字典",
    "group": "字典",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String[]",
            "optional": false,
            "field": "category_codes",
            "description": "<p>字典类型编号数组</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "请求例子:",
          "content": "{\n    \"category_codes\": [\"common_http_method\", \"sys_api_permission_type\"]\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>返回消息</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "code",
            "description": "<p>返回编号</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>返回数据</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data.Categorys",
            "description": "<p>字典类型对象</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "成功返回:",
          "content": "HTTP/1.1 200 OK\n{\n    \"Categorys\": {\n        \"common_http_method\": [\n            {\n                \"text\": \"GET\",\n                \"value\": 1\n            },\n            {\n                \"text\": \"POST\",\n                \"value\": 2\n            },\n            {\n                \"text\": \"PUT\",\n                \"value\": 3\n            },\n            {\n                \"text\": \"DELETE\",\n                \"value\": 4\n            }\n        ],\n        \"sys_api_permission_type\": [\n            {\n                \"text\": \"HTTP\",\n                \"value\": 1\n            },\n            {\n                \"text\": \"GQL\",\n                \"value\": 2\n            }\n        ]\n    }\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./get_dicts.go",
    "groupTitle": "字典"
  },
  {
    "type": "post",
    "url": "/bi/public/:code",
    "title": "执行报表",
    "sampleRequest": [
      {
        "url": "http://192.168.3.132:9102/"
      }
    ],
    "name": "执行报表",
    "group": "报表",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "code",
            "description": "<p>报表编号</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>返回消息</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "code",
            "description": "<p>返回编号</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>返回数据</p>"
          },
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "data.List",
            "description": "<p>列表</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "data.Count",
            "description": "<p>数量</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data.Detail",
            "description": "<p>详情</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n    \"msg\": \"请求成功\",\n    \"code\": 200,\n    \"data\": {\n        \"Count\": 1,\n        \"List\": [\n            {\n                \"name\": null,\n                \"organization_id\": \"testOrganization\"\n            }\n        ]\n    }\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./exec_report.go",
    "groupTitle": "报表"
  },
  {
    "type": "get",
    "url": "/bi/public/:code",
    "title": "根据编号获取报表信息",
    "sampleRequest": [
      {
        "url": "http://192.168.3.132:9102/"
      }
    ],
    "name": "获取报表信息",
    "group": "报表",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "code",
            "description": "<p>报表编号</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>返回消息</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "code",
            "description": "<p>返回编号</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>返回数据</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "data.display_type",
            "description": "<p>展示类型 ,1-列表,2-分页</p>"
          },
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "data.page_support",
            "description": "<p>是否支持分页，如果为true，则请求报表时候，需要额外加上currentPage和pageSize入参</p>"
          },
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "data.forms",
            "description": "<p>表单数组</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.forms.form_name",
            "description": "<p>表单名称(中文)</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "data.forms.form_type",
            "description": "<p>表单类型,1-文字输入框,2-数字输入框,3-日期输入框,4-select选择器</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.forms.attribute_name",
            "description": "<p>属性名称，请求报表时候的入参</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.forms.extra_config",
            "description": "<p>额外配置</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.forms.index_num",
            "description": "<p>顺序</p>"
          },
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "data.forms.dicts",
            "description": "<p>字典列表</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.forms.dicts.text",
            "description": "<p>字典的中文介绍</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "data.forms.dicts.value",
            "description": "<p>字典的值</p>"
          },
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "data.results",
            "description": "<p>报表返回结果信息数组</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.results.result_name",
            "description": "<p>返回信息名称(中文)</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "data.results.result_type",
            "description": "<p>返回信息类型, 1-文本，2-Date，3-Time，4-DateTime，5-字典，6-数字，7-百分比</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.results.attribute_name",
            "description": "<p>属性名称</p>"
          },
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "data.results.is_show",
            "description": "<p>是否展示</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.results.extra_config",
            "description": "<p>额外配置</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "data.results.index_num",
            "description": "<p>顺序</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.results.empty_value_font_color",
            "description": "<p>空值字体颜色</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.results.empty_value",
            "description": "<p>空值颜色</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.results.font_color",
            "description": "<p>字体颜色</p>"
          },
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "data.results.is_jump",
            "description": "<p>是否跳转</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "data.results.jump_type",
            "description": "<p>跳转类型,1-报表,2-链接(系统内), 3-外部链接</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.results.jump_target",
            "description": "<p>跳转目标</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "data.results.jump_mode",
            "description": "<p>跳转方式,1-对话框,2-标签</p>"
          },
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "data.results.jump_params",
            "description": "<p>跳转参数</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "data.results.jump_params.param_type",
            "description": "<p>参数类型,1-固定值，2-返回字段</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.results.jump_params.name",
            "description": "<p>参数名称</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.results.jump_params.value",
            "description": "<p>值</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"msg\": \"请求成功\",\n  \"code\": 200,\n  \"data\": {\n    \"display_type\": 2,\n    \"page_support\": true,\n    \"forms\": [\n      {\n        \"form_name\": \"机构名称\",\n        \"form_type\": 1,\n        \"attribute_name\": \"organization_name\"\n      },\n      {\n        \"form_name\": \"机构类别\",\n        \"form_type\": 2,\n        \"attribute_name\": \"organization_category\"\n      }\n    ],\n    \"results\": [\n      {\n        \"result_name\": \"organization_id\",\n        \"result_type\": 1,\n        \"attribute_name\": \"organization_id\"\n      },\n      {\n        \"result_name\": \"name\",\n        \"result_type\": 1,\n        \"attribute_name\": \"name\"\n      }\n    ]\n  }\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./get_report.go",
    "groupTitle": "报表"
  }
] });
