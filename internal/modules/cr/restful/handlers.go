package restful

import (
	"gitee.com/shiqiyue/xd-bi/internal/modules/cr/srv"
	"gorm.io/gorm"
)

type Handlers struct {
	Db                    *gorm.DB                   `inject:""`
	ReportSrv             *srv.ReportSrv             `inject:""`
	ReportFormSrv         *srv.ReportFormSrv         `inject:""`
	SqlReportSrv          *srv.SqlReportSrv          `inject:""`
	ReportResultColumnSrv *srv.ReportResultColumnSrv `inject:""`
	ReportDbSrv           *srv.ReportDbSrv           `inject:""`
	DictSrv               *srv.DictSrv               `inject:""`
	AreaSrv               *srv.AreaSrv               `inject:""`
}
