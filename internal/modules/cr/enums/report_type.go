package enums

const (
	// 报表类型SQL
	REPORT_TYPE_SQL = iota + 1
	// 报表类型 聚合
	REPORT_TYPE_AGGREGATE
)
