package enums

// 报表-返回类型
type biResultType int

var BiResultType biResultType

// 文本
func (c *biResultType) Text() int {
	return 1
}

// DATE
func (c *biResultType) Date() int {
	return 2
}

// TIME
func (c *biResultType) Time() int {
	return 3
}

// DATE_TIME
func (c *biResultType) DateTime() int {
	return 4
}

// 字典
func (c *biResultType) Dict() int {
	return 5
}

// 数字
func (c *biResultType) Number() int {
	return 6
}

// 百分比
func (c *biResultType) Percent() int {
	return 7
}

// 图片
func (c *biResultType) Image() int {
	return 8
}

// 布尔值
func (c *biResultType) Bool() int {
	return 9
}

// 评分(☆)
func (c *biResultType) ScoreStar() int {
	return 10
}

// 多张照片
func (c *biResultType) Images() int {
	return 11
}

// 数字数组
func (c *biResultType) Numbers() int {
	return 12
}

// 文本数组
func (c *biResultType) Texts() int {
	return 13
}

func (c *biResultType) ToText(value int) string {
	switch value {

	case 1:
		return "文本"

	case 2:
		return "DATE"

	case 3:
		return "TIME"

	case 4:
		return "DATE_TIME"

	case 5:
		return "字典"

	case 6:
		return "数字"

	case 7:
		return "百分比"

	case 8:
		return "图片"

	case 9:
		return "布尔值"

	case 10:
		return "评分(☆)"

	case 11:
		return "多张照片"

	case 12:
		return "数字数组"

	case 13:
		return "文本数组"

	}
	return ""
}

func (c *biResultType) ToValue(text string) int {
	switch text {

	case "文本":
		return 1

	case "DATE":
		return 2

	case "TIME":
		return 3

	case "DATE_TIME":
		return 4

	case "字典":
		return 5

	case "数字":
		return 6

	case "百分比":
		return 7

	case "图片":
		return 8

	case "布尔值":
		return 9

	case "评分(☆)":
		return 10

	case "多张照片":
		return 11

	case "数字数组":
		return 12

	case "文本数组":
		return 13

	}
	return 0
}
