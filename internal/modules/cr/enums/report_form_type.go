package enums

const (
	// 表单类型-文字输入框
	REPORT_FORM_TYPE_TEXT = iota + 1
	// 表单类型-数字输入框
	REPORT_FORM_TYPE_NUMBER
	// 表单类型-日期输入框
	REPORT_FORM_TYPE_DATE
	// select选择器
	REPORT_FORM_TYPE_SELECT
)
