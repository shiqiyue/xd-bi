package enums

import "gitee.com/lailonghui/vehicle-supervision-framework/pkg/dicts"

const (
	// 参数-文本
	REPORT_PARAM_TYPE_TEXT = iota + 1
	// 参数-数字
	REPORT_PARAM_TYPE_NUMBER
	// 参数-日期
	REPORT_PARAM_TYPE_DATE
	// 参数-文本数组
	REPORT_PARAM_TYPE_TEXT_ARRAY
)



// 报表-请求参数类型
type biReportParamType int

var BiReportParamType biReportParamType


// 文本
func (c * biReportParamType) Text()int{
	return 1
}

// 数字
func (c * biReportParamType) Number()int{
	return 2
}

// 日期
func (c * biReportParamType) Datetime()int{
	return 3
}

// 省市县对象
func (c * biReportParamType) ProvinceCityArea()int{
	return 9
}

// Any
func (c * biReportParamType) Select()int{
	return 4
}

// 文本
func (c * biReportParamType) District()int{
	return 13
}

// Any
func (c * biReportParamType) Hidden()int{
	return 5
}

// 文本
func (c * biReportParamType) Report()int{
	return 10
}

// 日期
func (c * biReportParamType) Date()int{
	return 6
}

// 文本
func (c * biReportParamType) Province()int{
	return 11
}

// 日期
func (c * biReportParamType) Time()int{
	return 7
}

// 数组
func (c * biReportParamType) MultiSelect()int{
	return 8
}

// 文本
func (c * biReportParamType) City()int{
	return 12
}


func (c *biReportParamType) ToText(value int) string{
	switch value {

	case 1:
		return "文本"

	case 2:
		return "Int"

	case 3:
		return "DATE_TIME"

	case 9:
		return "Object(省市县对象)"

	case 4:
		return "Any"

	case 13:
		return "文本"

	case 5:
		return "Any"

	case 10:
		return "文本"

	case 6:
		return "DATE"

	case 11:
		return "文本"

	case 7:
		return "TIME"

	case 8:
		return "数组"

	case 12:
		return "文本"

	}
	return ""
}


func (c *biReportParamType) ToDict(value int) *dicts.IntDict {
	return &dicts.IntDict{
		Text:  c.ToText(value),
		Value: value,
	}
}
