package enums

const (
	// 展示类型-列表
	REPORT_DISPLAY_TYPE_LIST = iota + 1
	// 展示类型-分页
	REPORT_DISPLAY_TYPE_PAGE
	// 展示类型-单个记录
	REPORT_DISPLAY_TYPE_DETAIL
)
