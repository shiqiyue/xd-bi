package enums

const (
	// 未启用
	REPORT_STATUS_PENDING = iota + 1
	// 启用
	REPORT_STATUS_ACTIVE
	// 废弃
	REPORT_STATUS_DISMISS
)
