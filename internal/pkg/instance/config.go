package instance

import (
	"gitee.com/lailonghui/vehicle-supervision-framework/pkg/apms"
	"gitee.com/lailonghui/vehicle-supervision-framework/pkg/loggers/rabbit_zap"
	"gitee.com/shiqiyue/xd-bi/configs"
	"github.com/spf13/viper"
)

var (
	// 日志收集器-配置信息
	loggerCollectorConfig *rabbit_zap.LoggerCollectorConfig
	// 全局变量-viper实例
	gViper *viper.Viper
	// 全局变量-配置信息
	gConfig *configs.Config
	// 服务发现-viper实例
	srvAddressViper *viper.Viper
	// 权限-viper实例
	authViper *viper.Viper
	// apm-viper实例
	apmViper *viper.Viper
	// apm-配置信息
	apmConfig *apms.ApmConfig
	// 日志收集器-配置信息viper实例
	loggerCollectorViper *viper.Viper
)

func SetViper(inp *viper.Viper) {
	gViper = inp
}

func GetViper() *viper.Viper {
	return gViper
}

func SetConfig(inp *configs.Config) {
	gConfig = inp
}

func GetConfig() *configs.Config {
	return gConfig
}

func SetSrvAddressViper(inp *viper.Viper) {
	srvAddressViper = inp
}

func GetSrvAddressViper() *viper.Viper {
	return srvAddressViper
}

func SetAuthViper(inp *viper.Viper) {
	authViper = inp
}

func GetAuthViper() *viper.Viper {
	return authViper
}

func SetApmViper(inp *viper.Viper) {
	apmViper = inp
}

func GetApmViper() *viper.Viper {
	return apmViper
}

func SetApmConfig(inp *apms.ApmConfig) {
	apmConfig = inp
}

func GetApmConfig() *apms.ApmConfig {
	return apmConfig
}

func SetLoggerCollectorConfig(inp *rabbit_zap.LoggerCollectorConfig) {
	loggerCollectorConfig = inp
}

func GetLoggerCollectorConfig() *rabbit_zap.LoggerCollectorConfig {
	return loggerCollectorConfig
}

func GetAppName() string {
	return "bi"
}
