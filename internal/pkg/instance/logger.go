package instance

import (
	kitLog "github.com/go-kit/kit/log"
	"go.uber.org/zap"
)

var (
	// 全局变量- gokit日志组件
	kLog kitLog.Logger

	// 全局变量- zap日志组件
	appLog *zap.Logger
)

func GetKitLog() kitLog.Logger {
	return kLog
}

func SetKitLog(inp kitLog.Logger) {
	kLog = inp
}

func GetAppLog() *zap.Logger {
	return appLog
}

func SetAppLog(inp *zap.Logger) {
	appLog = inp
}
