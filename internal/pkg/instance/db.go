package instance

import "gorm.io/gorm"

var (
	// db实例
	db *gorm.DB
)

func SetDb(i *gorm.DB) {
	db = i
}

func GetDb() *gorm.DB {
	return db
}
