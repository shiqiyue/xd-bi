module gitee.com/shiqiyue/xd-bi

go 1.14

require (
	gitee.com/lailonghui/vehicle-supervision-framework v0.2.90
	gitee.com/shiqiyue/bi-client v0.0.4
	gitee.com/shiqiyue/gqlgenc v0.0.4
	github.com/99designs/gqlgen v0.13.0
	github.com/Yamashou/gqlgenc v0.0.0-20210510074302-ec19415c7875 // indirect
	github.com/antonmedv/expr v1.8.9
	github.com/deckarep/golang-set v1.7.1
	github.com/dgryski/trifles v0.0.0-20200323201526-dd97f9abfb48
	github.com/gin-gonic/gin v1.6.3
	github.com/go-kit/kit v0.10.0
	github.com/grpc-ecosystem/go-grpc-middleware v1.0.1-0.20190118093823-f849b5445de4
	github.com/json-iterator/go v1.1.12
	github.com/mitchellh/mapstructure v1.4.1
	github.com/opentracing/opentracing-go v1.2.0
	github.com/rs/cors v1.7.0
	github.com/rs/xid v1.2.1
	github.com/spf13/pflag v1.0.5
	github.com/spf13/viper v1.7.1
	github.com/stretchr/testify v1.7.0
	github.com/vektah/gqlparser/v2 v2.1.0
	github.com/xwb1989/sqlparser v0.0.0-20180606152119-120387863bf2
	go.uber.org/zap v1.16.0
	golang.org/x/net v0.0.0-20210423184538-5f58ad60dda6
	google.golang.org/grpc v1.37.0
	gorm.io/datatypes v1.0.1
	gorm.io/driver/postgres v1.0.8
	gorm.io/gorm v1.21.14
	gorm.io/plugin/opentracing v0.0.0-20210506132430-24a9caea7709
)

replace google.golang.org/grpc => google.golang.org/grpc v1.36.0
