package configs

import (
	"fmt"
	"time"
)

// 配置信息
type Config struct {
	Gql     Http
	Restful Http
	Grpc    Grpc
	Log     Log
	Db      Db
	Xid     Xid
	App     App
	Dev     Dev
}

type Dev struct {
	// 是否强制使用预生产的数据库
	ForceStageDb bool
}

// xid配置
type Xid struct {
	Url string
}

// http 配置
type Http struct {
	// 绑定地址
	Address string
	// 跨域配置
	Cors Cors
}

// 应用配置
type App struct {
	// 数据库密码加密私钥
	DbPasswordPrivateKey string
}

// 跨域配置
type Cors struct {
	// 允许的origin
	AllowedOrigins []string

	// 允许的头部
	AllowedHeaders []string
}

// grpc 配置
type Grpc struct {
	// 绑定地址
	Address string
}

// 日志配置
type Log struct {
	// 日志模式, 支持 console,file,all三种
	Mode string
	// 日志级别, 支持 debug, info ,error ,warn
	Level string
	// 日志文件路径
	File LogFile
}

// 日志-文件配置
type LogFile struct {
	// 文件路径
	Path string
}

//Db 数据库配置
type Db struct {
	// 主节点
	Master DbNode `yaml:"master"`
	// 从节点
	Slaves []DbNode `yaml:"slaves"`
	//DbPool 连接池设置
	DbPool `yaml:"pool"`
}

// DbNode 数据库节点配置
type DbNode struct {
	//Host 数据库连接地址
	Host string `yaml:"host"`
	//Port 数据库端口
	Port int `yaml:"port"`
	//Username 用户名
	Username string `yaml:"username"`
	//Password 密码
	Password string `yaml:"password"`
	//DbName 数据库连接
	Dbname string `yaml:"dbname"`
	//SslMode ssl模式
	SslMode string `yaml:"sslmode"`
	//TimeZone 时区
	Timezone string `yaml:"timezone"`
}

func (i DbNode) GetDsn() string {
	return fmt.Sprintf("host=%s user=%s password=%s dbname=%s port=%d sslmode=%s TimeZone=%s",
		i.Host,
		i.Username,
		i.Password,
		i.Dbname,
		i.Port,
		i.SslMode,
		i.Timezone)
}

//DbPool 数据库pool配置
type DbPool struct {
	//MaxIdleConn 最多空闲连接
	MaxIdleConn int `yaml:"maxIdleConn"`
	//MaxOpenConn 最多连接数
	MaxOpenConn int `yaml:"maxOpenConn"`
	//MaxLifeTime 连接多久过期
	MaxLifeTime time.Duration `yaml:"maxLifeTime"`
}
