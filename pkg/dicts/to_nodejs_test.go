package dicts

import (
	"fmt"
	data_sys "gitee.com/shiqiyue/xd-bi/internal/client/data-sys"
	"testing"
)

func TestToNodejs(t *testing.T) {
	c := data_sys.DictionaryCategory{CategoryName: "测试", CategoryCode: "test"}
	ds := []data_sys.Dictionary{}
	r := "图片"
	ds = append(ds, data_sys.Dictionary{Name: "image", Remarks: &r, Value: 1})
	ds = append(ds, data_sys.Dictionary{Name: "text", Remarks: &r, Value: 2})

	ds = append(ds, data_sys.Dictionary{Name: "number", Remarks: &r, Value: 3})

	ds = append(ds, data_sys.Dictionary{Name: "c", Remarks: &r, Value: 4})
	toGo := ToNodejs(c, ds)
	fmt.Println(toGo)

}
