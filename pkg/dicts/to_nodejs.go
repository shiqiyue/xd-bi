package dicts

import (
	"gitee.com/shiqiyue/xd-bi/internal/modules/cr/model"
	"github.com/99designs/gqlgen/codegen/templates"
	"github.com/dgryski/trifles/uuid"
	"go.uber.org/zap/buffer"
	"log"
	"text/template"
)

func ToNodejs(category model.DictCategory, dicts []*model.Dict) string {
	m := map[string]interface{}{}
	m["CategoryRemarks"] = category.CategoryName
	m["CategoryGoPrivateName"] = templates.ToGoPrivate(category.CategoryCode)
	m["CategoryGoName"] = templates.ToGo(category.CategoryCode)
	ds := []map[string]interface{}{}
	for _, dict := range dicts {
		d := map[string]interface{}{}
		d["Remarks"] = dict.Remarks
		d["GoName"] = dict.Name
		d["GoPrivateName"] = templates.ToGoPrivate(dict.Name)
		d["Value"] = dict.Value
		ds = append(ds, d)
	}
	m["Dicts"] = ds
	t, err := template.New(uuid.UUIDv4()).Parse(nodejs_t)
	if err != nil {
		log.Fatal(err)
	}
	bs := buffer.Buffer{}
	err = t.Execute(&bs, m)
	if err != nil {
		log.Fatal(err)
	}
	return bs.String()
}

var nodejs_t = `
const enums = {
{{ range $dict := .Dicts}}
  {{$dict.GoName}}: { 'text': '{{$dict.Remarks}}', 'value': {{$dict.Value}} },
{{end}}
}
export default enums

`
