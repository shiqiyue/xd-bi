package dicts

import (
	model1 "gitee.com/shiqiyue/xd-bi/internal/modules/cr/model"
	"github.com/99designs/gqlgen/codegen/templates"
	"github.com/dgryski/trifles/uuid"
	"go.uber.org/zap/buffer"
	"log"
	"text/template"
)

func ToGo(category model1.DictCategory, dicts []*model1.Dict) string {
	m := map[string]interface{}{}
	m["CategoryRemarks"] = category.CategoryName
	m["CategoryGoPrivateName"] = templates.ToGoPrivate(category.CategoryCode)
	m["CategoryGoName"] = templates.ToGo(category.CategoryCode)
	ds := []map[string]interface{}{}
	for _, dict := range dicts {
		d := map[string]interface{}{}
		d["Remarks"] = dict.Remarks
		d["GoName"] = templates.ToGo(dict.Name)
		d["GoPrivateName"] = templates.ToGoPrivate(dict.Name)
		d["Value"] = dict.Value
		ds = append(ds, d)
	}
	m["Dicts"] = ds
	t, err := template.New(uuid.UUIDv4()).Parse(t)
	if err != nil {
		log.Fatal(err)
	}
	bs := buffer.Buffer{}
	err = t.Execute(&bs, m)
	if err != nil {
		log.Fatal(err)
	}
	return bs.String()
}

var t = `
{{$StructName := .CategoryGoPrivateName }}
// {{.CategoryRemarks}}
type {{$StructName}} int

var {{.CategoryGoName}} {{$StructName}}

{{ range $dict := .Dicts }}
// {{$dict.Remarks}}
func (c * {{$StructName}}) {{$dict.GoName}}()int{
	return {{$dict.Value}}
}
{{end}}

func (c *{{$StructName}}) ToText(value int) string{
	switch value {
{{ range $dict := .Dicts }}
		case {{$dict.Value}}:
			return "{{$dict.Remarks}}"
{{end}}
	}
	return ""
}


func (c *{{$StructName}}) ToDict(value int) *dicts.IntDict {
	return &dicts.IntDict{
		Text:  c.ToText(value),
		Value: value,
	}
}

func (c *{{$StructName}}) ToValue(text string) int{
	switch text {
{{ range $dict := .Dicts }}
		case "{{$dict.Remarks}}":
			return {{$dict.Value}}
{{end}}
	}
	return 0
}
`
