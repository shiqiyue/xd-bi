package middle

import (
	"context"
	"fmt"
	"gitee.com/lailonghui/vehicle-supervision-framework/pkg/loggers"
	"github.com/99designs/gqlgen/graphql"
)

func LoggerMiddle(ctx context.Context, next graphql.ResponseHandler) *graphql.Response {
	response := next(ctx)
	errors := graphql.GetErrors(ctx)
	if errors != nil && len(errors) > 0 {
		for _, e := range errors {
			if e != nil {
				loggers.Error(fmt.Sprintf("%+v\n", e.Unwrap()), ctx)

			}
		}
	}

	return response

}
