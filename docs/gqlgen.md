## 如何开发gqlgen

### gqlgen接口定义
进入pkg/modules/模块名/gqlgen/服务名 目录
创建gqlgen.yml,例如
```yaml

# Where are all the schema files located? globs are supported eg  src/**/*.graphqls
schema:
  - ./*.graphqls

# Where should the generated server code go?
exec:
  filename:  ../../../../../internal/modules/adm/gqlgen/enterprise/gqlgened.go
  package: enterprise

# Uncomment to enable federation
# federation:
#   filename: graph/generated/federation.go
#   package: generated

# Where should any generated models go?
model:
  filename: models_gen.go
  package: enterprise

# Where should the resolver implementations go?
resolver:
  layout: follow-schema
  dir: ../../../../../internal/modules/adm/gqlgen/enterprise
  package: enterprise

```
创建graphqls文件，例如
```graphql
type Enterprise{
    enterprise_id: String!
    name: String
}


type Query{
    getById(enterpriseId: String): Enterprise
    getById2(enterpriseId: String): Enterprise
    getById3(enterpriseId: String): Enterprise
}
```


### 生成gql代码

创建gen.go文件，例如
```
package enterprise

//go:generate go run github.com/99designs/gqlgen generate.

```
执行gen.go里面的go generate

### 实现gql
进入internal/modules/模块名/gqlgen/服务名 目录,编写完善resolver

### 注册gql到路由
进入internal/setup.gin.go，修改routerRegister函数
