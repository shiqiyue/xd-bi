
## 不代理gitee
goproxy代理gitee可能会出现404问题，所以设置不代理gitee
```text
 go env -w GOPRIVATE=gitee.com,*.gitee.com
```



## 安装protoc和grpc
```text
1.到下面网站，下载protoc可执行文件，文件名 protoc-x.xx.x-win64.zip
https://github.com/protocolbuffers/protobuf/releases
2.执行以下指令
go get -u github.com/golang/protobuf/{proto,protoc-gen-go}
go get -u google.golang.org/grpc
```


## 更换module名称
```text
修改go.mod文件里面的module，修改为 github.com/shinedone/srv-微服务名称,比如github.com/shinedone/srv-org
全局替换module名
```
