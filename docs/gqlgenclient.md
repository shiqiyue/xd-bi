```text
gqlgen客户端使用开源项目进行生成
https://github.com/Yamashou/gqlgenc

第一步
go get -u github.com/Yamashou/gqlgenc

第二步
复制例子 internal/client/data-adm/department里面的 
gen.go
gqlgenc.yml

第三步
新建客户端目录，将上面两个文件放进去

第四部
在客户端目录下新建query目录，放置请求使用的gql

第五步
执行gen.go

第六步
到internal/setup/client文件中，注册client到beans中
```
