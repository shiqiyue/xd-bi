# 简介
```text
这是业务层的服务模板。
```
![业务层](../assets/业务层.png)


```text
上图为业务层请求示意图。
服务一般提供三种请求方式rest,gql,grpc。
rest,gql提供给前端调用。
gql和grpc提供给后端服务之间调用。
gql的resolver、grpc的service、rest的handler 相当于控制器，一般不负责处理业务代码；
业务代码的处理放在services里面处理。
services需要调用外部服务时候，使用client进行调用
client一般由相关服务的开发者提供，client的模板地址是: https://gitee.com/shiqiyue/go-srv-client.git

```

```text
项目的目录结构，url的组成都围绕着三个元素；服务，模块，协议。
项目的业务代码一般都是写在internal目录里面，相关的http,grpc代码都是写在对应的 [internal/modules/模块/协议] 目录下
http的path组成一般是 【/服务/模块/协议/其他】

```



### 相关工程
[框架](https://gitee.com/shiqiyue/go-srv-framework)  
[服务客户端](https://gitee.com/shiqiyue/go-srv-client)
