
# 服务层框架更新方式
修改go.mod里面的gitee.com/shiqiyue/go-srv-framework 的版本号，包括replace的
比如原来是
```
require gitee.com/shiqiyue/go-srv-framework v0.0.3
replace gitee.com/shiqiyue/go-srv-framework => gitee.com/shiqiyue/go-srv-framework v0.0.3

```
要升级至0.0.5版本，则修改如下
```
require gitee.com/shiqiyue/go-srv-framework v0.0.5
replace gitee.com/shiqiyue/go-srv-framework => gitee.com/shiqiyue/go-srv-framework v0.0.5

```
