
## 项目结构
[标准布局](standard_package.md)  
下面介绍一下internal包的结构

## internal/global
这个包是放置全局变量或者工具类

## internal/global/instance
这个包是放置全局的变量，现在放了配置变量和日志变量

## internal/modules
这个包是用来放置各个业务模块的代码

## internal/setup
这个包是用来初始化程序的
