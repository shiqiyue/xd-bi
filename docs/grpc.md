
## 如何开发grpc
### proto文件放置的目录
进入pkg/modules/模块名/grpc/服务名 目录
### proto文件编写
编写proto文件，类似以下enterprise.proto
``` protobuf
syntax = "proto3";
package enterprise;

import "google/protobuf/timestamp.proto";

option go_package = "github.com/shinedone/srv-server/pkg/adm/grpc/enterprise";


service EnterpriseSrv{
  rpc GetByEnterpriseId (GetByEnterpriseIdReq) returns (GetByEnterpriseIdResp);
  rpc GetByEnterpriseId2 (GetByEnterpriseIdReq) returns (GetByEnterpriseIdResp);
}

message GetByEnterpriseIdReq{
  string enterprise_id = 1;
}

message GetByEnterpriseIdResp{
  string enterprise_id = 1;
  string name = 2;
  google.protobuf.Timestamp created_at = 3;

}

```
### proto生成代码
创建gen.go,执行gen.go文件里面的 go:generate
```go
package enterprise

//go:generate protoc --go_opt=paths=source_relative --go_out=plugins=grpc:.  *.proto

```


### grpc实现
进入 internal/modules/模块名/grpc/服务名 目录, 新建service.go文件，编写生成的grpc实现
```go

type grpcService struct {
	enterpriseSrv *service.EnterpriseSrv
}

func NewGrpcServer(enterpriseSrv *service.EnterpriseSrv) pd.EnterpriseGrpcServer {
	return grpcService{enterpriseSrv: enterpriseSrv}
}

func (e grpcService) GetByEnterpriseId(ctx context.Context, req *pd.GetByEnterpriseIdReq) (*pd.GetByEnterpriseIdResp, error) {
	return &pd.GetByEnterpriseIdResp{
		EnterpriseId: "dasd",
		Name:         "dasd",
	}, nil
}

func (e grpcService) GetByEnterpriseId2(ctx context.Context, req *pd.GetByEnterpriseIdReq) (*pd.GetByEnterpriseIdResp, error) {
	return &pd.GetByEnterpriseIdResp{
		EnterpriseId: "dasd",
		Name:         "dasd",
	}, nil
}

```
### 注册grpc
修改internal/setup/grpc.go文件，在registerGrpcServer函数中加入上面编写的实现
