
```text
微服务之间通过grpc或者http进行调用。
相关的接口文件（服务的定义，请求参数，返回参数）需放置到pkg包里面。
比如modules/adm/gqlgen/enterprise 包 和 modules/adm/grpc/enterprise 包。
相应的客户端也可以在pkg里面提供。
```

## 如何调用其他微服务
```text
在go.mod里面加上对应微服务的依赖
使用依赖里面的提供的客户端或者按需求自己编写客户端
```
