package main

import (
	"context"
	"gitee.com/shiqiyue/xd-bi/internal/setup/cr"
)

func main() {
	bgCtx := context.Background()
	errCh := cr.InitSetup(bgCtx)

	<-errCh

}
