module.exports = {
  devServer: {
    port: 8082,
    proxy: {
      '/bi/mgr/gql': {
        target: 'http://localhost:9300'

      }
    }
  },
  publicPath: '/web'
}
