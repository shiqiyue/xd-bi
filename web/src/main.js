import Vue from 'vue'
import './plugins/axios'
import App from './App.vue'
import './plugins/element.js'
import router from './router'
Vue.config.productionTip = false
import * as filters from './filters' // global filters
import store from './store'

import VueClipboard from 'vue-clipboard2'

Vue.use(VueClipboard)
// register global utility filters.
Object.keys(filters).forEach(key => {
  Vue.filter(key, filters[key])
})

new Vue({
  router,
  store,

  render: h => h(App)
}).$mount('#app')

