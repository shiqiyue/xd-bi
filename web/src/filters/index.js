// set function parseTime,formatTime to filter
export { parseTime, parseDateTime, parseDate } from '@/utils'

/** **
 * 字典 filter，返回value对应的name,value为数字
 * @param value
 * @param dict
 */
export function dictFilter(value, dict, nullValue) {
  nullValue = nullValue || ''
  var cValue = value
  if (!isNaN(cValue)) {
    cValue = parseInt(cValue)
  }
  for (const item in dict) {
    if (dict[item].value === cValue) {
      return dict[item].text
    }
  }
  // for (let i = 0; i < dict.length; i++) {
  //   if (dict[i].value === value) {
  //     return dict[i].text
  //   }
  // }
  return nullValue
}

/** *
 * 字典filter,返回value对应的name,value为字符串
 * @param value
 * @param dict
 * @param nullValue
 */
export function stringDictFilter(value, dict, nullValue) {
  nullValue = nullValue || ''
  for (const item in dict) {
    if (dict[item].value.toLowerCase() === value.toLowerCase()) {
      return dict[item].text
    }
  }
  return nullValue
}

export function objectDictFilter(value, dict, nullValue) {
  nullValue = nullValue || ''
  if (dict[value] !== undefined) {
    return dict[value].text
  }
  return nullValue
}

export function moneyFilter(value) {
  var cValue = value
  if (!isNaN(cValue)) {
    cValue = parseInt(cValue)
  }
  return cValue / 100
}

/** *
 * 百分比过滤器
 * @param value
 */
export function percentFilter(value) {
  var cValue = value
  if (!isNaN(cValue)) {
    cValue = parseFloat(cValue)
  }
  return cValue * 100 + '%'
}
export function booleanFilter(value) {
  if (value) {
    return '是'
  }
  return '否'
}
