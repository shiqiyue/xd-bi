import gql from 'graphql-tag'
import apolloClient from '../utils/graphql'

function page(params) {
  return apolloClient.query({
    fetchPolicy: 'network-only',
    query: gql`query ($req: ReportDbPageReq!) {
            reportDbPage(Req: $req){
                TotalRecord
                Records{
                    ReportDbId
                    Name
                    MaxIdleConns
                    MaxOpenConns
                    ConnMaxLifetime
                    CreatedAt
                    UpdatedAt
                    Type
                }
            }
        }`,
    variables: { req: params }
  })
}

function list() {
  return apolloClient.query({
    fetchPolicy: 'network-only',
    query: gql`query {
            reportDbList{
                ReportDbId
                Name
            }
        }`
  })
}

function detail(params) {
  return apolloClient.query({
    fetchPolicy: 'network-only',
    query: gql`query ($id: String!) {
            reportDbDetail(DbId: $id){
                ReportDbId
                Name
                Host
                Port
                DbName
                Type
                Username
                MaxIdleConns
                MaxOpenConns
                ConnMaxLifetime
            }
        }`,
    variables: { id: params }
  })
}

// 添加报表
function add(params) {
  return apolloClient.mutate({
    mutation: gql`mutation ($req: InsertReportDbReq!) {
            insertReportDb(Req: $req)
        }`,
    variables: { req: params }
  })
}

function update(params) {
  return apolloClient.mutate({
    mutation: gql`mutation ($req: UpdateReportDbReq!) {
            updateReportDb(Req: $req)
        }`,
    variables: { req: params }
  })
}

export default {
  page,
  detail,
  add,
  update,
  list
}
