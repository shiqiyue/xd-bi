import gql from 'graphql-tag'
import apolloClient from '../utils/graphql'

// 报表列表
function getReportList(params) {
  return apolloClient.query({
    fetchPolicy: 'network-only',
    query: gql`query ($req: ReportPageReq!) {
            reportPage(Req: $req){
                TotalRecord
                Records{
                    ReportId
                    Name
                    Code
                    Status
                    Type
                    CreatedAt
                    UpdatedAt
                }
            }
        }`,
    variables: { req: params }
  })
}

// 报表详情
function getReportDetail(params) {
  return apolloClient.query({
    fetchPolicy: 'network-only',
    query: gql`query ($reportId: String!) {
            reportDetail(ReportId: $reportId){
                ReportId
                Code
                Type
                Name
                DisplayType
                DataAccessSupport
                PageSupport
                ShowSummary
            }
        }`,
    variables: { reportId: params }
  })
}

// 添加报表
function addReport(params) {
  return apolloClient.mutate({
    mutation: gql`mutation ($req: InsertReportReq!) {
            insertReport(Req: $req)
        }`,
    variables: { req: params }
  })
}

// 更新报表
function updateReport(params) {
  return apolloClient.mutate({
    mutation: gql`mutation ($req: UpdateReportReq!) {
            updateReport(Req: $req)
        }`,
    variables: { req: params }
  })
}

function enableReport(params) {
  return apolloClient.mutate({
    mutation: gql`mutation ($req: String!) {
            enableReport(ReportId: $req)
        }`,
    variables: { req: params }
  })
}

function dismissReport(params) {
  return apolloClient.mutate({
    mutation: gql`mutation ($req: String!) {
            dismissReport(ReportId: $req)
        }`,
    variables: { req: params }
  })
}

function updateReportForm(params) {
  return apolloClient.mutate({
    mutation: gql`mutation ($req: UpdateReportFormReq!) {
            updateReportForm(Req: $req)
        }`,
    variables: { req: params }
  })
}

function updateReportResultColumn(params) {
  return apolloClient.mutate({
    mutation: gql`mutation ($req: UpdateReportResultColumnReq!) {
            updateReportResultColumns(Req: $req)
        }`,
    variables: { req: params }
  })
}

function getReportFormList(params) {
  return apolloClient.query({
    fetchPolicy: 'network-only',
    query: gql`query ($reportId: String!) {
            reportDetail(ReportId: $reportId){
                ReportId
                ReportForms{
                    Name
                    Type
                    AttributeName
                    DictCategoryId
                    DictCategoryName
                    ReturnAllDict
                    ExtraConfig
                    DefaultValue
                    Required
                    QueryReportCode
                }
                ReportResultColumns{
                    ReportResultColumnId
                    Type
                    Name
                    AttributeName
                    DictCategoryId
                    DictCategoryName
                    ExtraConfig
                    IsShow
                    FontColor
                    EmptyValueFontColor
                    EmptyValue
                    IsJump
                    JumpType
                    JumpTarget
                    JumpMode
                    JumpParams{
                        ParamType
                        Name
                        Value
                    }
                }
            }
        }`,
    variables: { reportId: params }
  })
}

function reportDetailByCode(params) {
  return apolloClient.query({
    fetchPolicy: 'network-only',
    query: gql`query ($code: String!) {
            reportDetailByCode(Code: $code){
                ReportId
                Code
                Type
                SqlReport{
                    SqlReportId
                    Sql
                    DbId
                }
                ReportForms{
                    AttributeName
                    Name
                    Type
                }
                RequestReportParams{
                    AttributeName
                    Name
                    Type
                }
            }
        }`,
    variables: { code: params }
  })
}

function testReport(params) {
  return apolloClient.mutate({
    mutation: gql`mutation ($req: TestReportReq!) {
            testReport(Req: $req){
                List
                Count
                Sql
                Detail
                CountSql
                Aggregate
            }
        }`,
    variables: { req: params }
  })
}

export default {
  getReportList,
  getReportDetail,
  addReport,
  updateReport,
  enableReport,
  dismissReport,
  updateReportForm,
  getReportFormList,
  reportDetailByCode,
  updateReportResultColumn,
  testReport
}
