
const enums = {
  PENDING: { 'text': '未启用', 'value': 1 },
  ACTIVE: { 'text': '启用', 'value': 2 },
  DISMISS: { 'text': '废弃', 'value': 3 }
}
export default enums
