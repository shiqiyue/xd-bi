
const enums = {

  TEXT: { 'text': '文本', 'value': 1 },

  DATE: { 'text': 'DATE', 'value': 2 },

  TIME: { 'text': 'TIME', 'value': 3 },

  DATE_TIME: { 'text': 'DATE_TIME', 'value': 4 },

  DICT: { 'text': '字典', 'value': 5 },

  NUMBER: { 'text': '数字', 'value': 6 },

  PERCENT: { 'text': '百分比', 'value': 7 },

  IMAGE: { 'text': '图片', 'value': 8 },

  BOOL: { 'text': '布尔值', 'value': 9 },

  SCORE_STAR: { 'text': '评分(☆)', 'value': 10 },

  IMAGES: { 'text': '多张照片', 'value': 11 },

  NUMBERS: { 'text': '数字数组', 'value': 12 },

  TEXTS: { 'text': '文本数组', 'value': 13 },

  PERIOD: { 'text': '时间段', 'value': 14 },

  ATTACH_FILES: { 'text': '附件(文件)数组', 'value': 15 },

  ATTACH_FILE: { 'text': '附件(文件)', 'value': 16 },

  AREA_POINTS: { 'text': '区域/路线视图', 'value': 18 },

  SELF_CODE_STYLE: { 'text': '自编号样式', 'value': 19 }

}
export default enums

