
const enums = {
  TEXT: { 'text': '字符串', 'value': 1 },
  NUMBER: { 'text': '数字', value: 2 },
  DATETIME: { 'text': '日期', value: 3 },
  SELECT: { 'text': '数字', value: 4 },
  HIDDEN: { 'text': '字符串', 'value': 5 },

  DATE: { 'text': '日期', 'value': 6 },

  TIME: { 'text': '日期', 'value': 7 },

  MULTI_SELECT: { 'text': '数组', 'value': 8 },
  PROVINCE_CITY_AREA: { 'text': '省市县对象', 'value': 9, attributes: ['district_id', 'city_id', 'province_id'] }

}
export default enums
