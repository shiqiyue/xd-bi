
const enums = {

  REPORT: { 'text': '报表', 'value': 1 },

  LINK: { 'text': '链接(系统内)', 'value': 2 },

  EXTERNAL_LINK: { 'text': '外部链接', 'value': 3 }

}
export default enums

