
const enums = {

  TEXT: { 'text': '文字输入框', 'value': 1 },

  NUMBER: { 'text': '数字输入框', 'value': 2 },

  DATETIME: { 'text': '日期+时间选择框', 'value': 3 },

  SELECT: { 'text': '选择框（单选）', 'value': 4 },

  HIDDEN: { 'text': '隐藏', 'value': 5 },

  DATE: { 'text': '日期选择框', 'value': 6 },

  TIME: { 'text': '时间选择框', 'value': 7 },

  MULTI_SELECT: { 'text': '选择框-多选', 'value': 8 },

  PROVINCE_CITY_AREA: { 'text': '省市县', 'value': 9 },

  REPORT: { 'text': '报表', value: 10 },

  PROVINCE: { 'text': '省份', value: 11 },

  CITY: { 'text': '城市', value: 12 },

  DISTRICT: { 'text': '区县', value: 13 }

}
export default enums

