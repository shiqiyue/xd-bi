
const enums = {
  DATA_ACCESS_ALL: { 'text': '所有权限', 'value': 1 },
  DATA_ACCESS_PROVINCE: { 'text': '省', 'value': 2 },
  DATA_ACCESS_CITY: { 'text': '市', 'value': 3 },
  DATA_ACCESS_AREA: { 'text': '县', 'value': 4 },
  DATA_ACCESS_ORG: { 'text': '组织', 'value': 5 },
  DATA_ACCESS_DEPT: { 'text': '部门', 'value': 6 },
  DATA_ACCESS_USER: { 'text': '个人', 'value': 7 }
}
export default enums
