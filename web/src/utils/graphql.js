import ApolloClient from 'apollo-boost'
import store from '@/store'
const httpEndpoint = process.env.VUE_APP_GRAPHQL_HTTP || 'http://localhost:4000/graphql'

const apolloClient = new ApolloClient({
  // 你需要在这里使用绝对路径
  uri: httpEndpoint,
  tokenName: 'bi-token',
  headers: { 'Authorization': store.getters.token || '' }
})
export default apolloClient
