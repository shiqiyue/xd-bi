import miment from 'miment'

export function parseDate(time, cFormat) {
  if (arguments.length === 0) {
    return null
  }
  if (!time) {
    return null
  }
  var momentObj = miment(time)
  cFormat = cFormat || 'YYYY-MM-DD'
  return momentObj.format(cFormat)
}

export function parseDateTime(time, cFormat) {
  if (arguments.length === 0) {
    return null
  }
  if (!time) {
    return null
  }
  var momentObj = miment(time)
  cFormat = cFormat || 'YYYY-MM-DD hh:mm'
  return momentObj.format(cFormat)
}

export function parseTime(time, cFormat) {
  if (arguments.length === 0) {
    return null
  }
  if (!time) {
    return null
  }
  var momentObj = miment(time)
  cFormat = cFormat || 'HH:mm:ss'
  return momentObj.format(cFormat)
}
