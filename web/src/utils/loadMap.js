export default function(callback = () => {}) {
  if (!window.EV) {
    const script = document.createElement('script')
    script.src = 'http://a.qqearth.com:81/SE_JSAPI?v=ol&uid=qzzxwx'
    document.body.appendChild(script)
    script.onload = script.onreadystatechange = function() {
      const loaded =
                !this.readyState ||
                this.readyState === 'loaded' ||
                this.readyState === 'complete'
      if (loaded) {
        callback()
      }
    }
  } else {
    callback()
  }
}
