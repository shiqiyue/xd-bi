import toInt from 'licia/toInt'
import toNum from 'licia/toNum'
import isNumeric from 'licia/isNumeric'
import isInt from 'licia/isInt'

function isEmpty(value) {
  if (value === null || value === undefined) {
    return true
  }
  if (typeof (value) === 'string') {
    if (value.trim().length === 0) {
      return true
    }
  }
  return false
}

/** *
 * 正整数
 * @param rule
 * @param value
 * @param callback
 */
function positiveInt(rule, value, callback) {
  if (isEmpty(value)) {
    callback()
  }
  if (!isNumeric(value)) {
    callback(new Error('只能为数字'))
    return
  }
  if (!isInt(parseFloat(value))) {
    callback(new Error('只能为整数'))
    return
  }
  var iValue = toInt(value)
  if (iValue < 0) {
    callback(new Error('不能小于0'))
    return
  }
  callback()
}

/** *
 * 正数
 * @param rule
 * @param value
 * @param callback
 */
function positiveNum(rule, value, callback) {
  if (isEmpty(value)) {
    callback()
  }
  if (!isNumeric(value)) {
    callback(new Error('只能为数字'))
    return
  }
  var iValue = toNum(value)
  if (iValue < 0) {
    callback(new Error('不能小于0'))
    return
  }
  callback()
}

/** *
 * 小于100
 * @param rule
 * @param value
 * @param callback
 */
function lessThan100(rule, value, callback) {
  if (isEmpty(value)) {
    callback()
  }
  if (!isNumeric(value)) {
    callback(new Error('只能为数字'))
    return
  }
  var iValue = toNum(value)
  if (iValue >= 100) {
    callback(new Error('不能大于等于100'))
    return
  }
  callback()
}

/** *
 * 数字最多小数点后两位
 * @param rule
 * @param value
 * @param callback
 */
function numScale2(rule, value, callback) {
  if (isEmpty(value)) {
    callback()
  }
  if (!isNumeric(value)) {
    callback(new Error('只能为数字'))
    return
  }
  var iValue = toNum(value)
  var s = iValue.toString().split('.')[1]
  if (s !== undefined && s.length > 2) {
    callback(new Error('小数点后最多两位'))
    return
  }
  callback()
}

export default {
  positiveInt,
  positiveNum,
  lessThan100,
  numScale2
}
