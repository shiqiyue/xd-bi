import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

/** note: Submenu only appear when children.length>=1
 *  detail see  https://panjiachen.github.io/vue-element-admin-site/guide/essentials/router-and-nav.html
 **/

/**
 * hidden: true                   if `hidden:true` will not show in the sidebar(default is false)
 * alwaysShow: true               if set true, will always show the root menu, whatever its child routes length
 *                                if not set alwaysShow, only more than one route under the children
 *                                it will becomes nested mode, otherwise not show the root menu
 * redirect: noredirect           if `redirect:noredirect` will no redirect in the breadcrumb
 * name:'router-name'             the name is used by <keep-alive> (must set!!!)
 * meta : {
    roles: ['admin','editor']     will control the page roles (you can set multiple roles)
    title: 'title'               the name show in submenu and breadcrumb (recommend set)
    icon: 'svg-name'             the icon show in the sidebar,
    noCache: true                if true ,the page will no be cached(default is false)
  }
 **/
export const constantRouterMap = [

  {
    path: '/',
    component: () => import('@/views/report/list')
  },
  {
    path: '/report/add',
    component: () => import('@/views/report/add')
  },
  {
    path: '/report/edit/:id',
    props: true,
    component: () => import('@/views/report/edit')
  },
  {
    path: '/reportDb/list',
    component: () => import('@/views/db/list')
  },
  {
    path: '/reportDb/add',
    component: () => import('@/views/db/add')
  },
  {
    path: '/reportDb/edit/:id',
    props: true,
    component: () => import('@/views/db/edit')
  },
  {
    path: '/report/editForm/:id',
    props: true,
    component: () => import('@/views/report/edit_form')
  },
  {
    path: '/report/editSql/:id',
    props: true,
    component: () => import('@/views/report/edit_sql')
  },
  {
    path: '/report/editResult/:id',
    props: true,
    component: () => import('@/views/report/edit_result')
  },
  {
    path: '/report/editAggregate/:id',
    props: true,
    component: () => import('@/views/report/edit_aggreate')
  },

  {
    path: '/report/test/json/:code',
    props: true,
    component: () => import('@/views/test/json')
  },
  {
    path: '/report/apidoc/:id',
    props: true,
    component: () => import('@/views/report/api_doc')
  },
  {
    path: '/report/test/json',
    props: true,
    component: () => import('@/views/test/json')
  },
  {
    path: '/dictCategory/list',
    component: () => import('@/views/dict/category/list')
  },
  {
    path: '/dictCategory/add',
    component: () => import('@/views/dict/category/add')
  },
  {
    path: '/dictCategory/edit/:id',
    props: true,
    component: () => import('@/views/dict/category/edit')
  },
  {
    path: '/dictCategory/editSql/:id',
    props: true,
    component: () => import('@/views/dict/category/edit_sql')
  },
  {
    path: '/dict/list/:dictCategoryId',
    props: true,
    component: () => import('@/views/dict/list')
  },
  {
    path: '/dict/add/:dictCategoryId',
    props: true,
    component: () => import('@/views/dict/add')
  },
  {
    path: '/dict/edit/:id',
    props: true,
    component: () => import('@/views/dict/edit')
  },
  {
    path: '/tools/error_log_format',
    props: true,
    component: () => import('@/views/tools/error_log_format')
  },
  {
    path: '/tools/map_tool',
    props: true,
    component: () => import('@/views/tools/map_tool')
  }
]

export default new Router({
  // mode: 'history', // require service support
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRouterMap
})

