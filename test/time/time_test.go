package time

import (
	"fmt"
	"testing"
	"time"
)

func TestSub(t *testing.T) {
	now := time.Now()
	time.Sleep(time.Second)
	d := time.Since(now)
	fmt.Println(d/time.Millisecond)
	fmt.Println(int64(d/time.Millisecond))
}
