package grpc

import (
	"context"
	"fmt"
	"gitee.com/lailonghui/vehicle-supervision-framework/pkg/grpcs"
	"gitee.com/lailonghui/vehicle-supervision-framework/pkg/loggers"
	"gitee.com/shiqiyue/bi-client"
	grpc_middleware "github.com/grpc-ecosystem/go-grpc-middleware"
	grpc_zap "github.com/grpc-ecosystem/go-grpc-middleware/logging/zap"
	"github.com/stretchr/testify/assert"
	"go.uber.org/zap/zapcore"
	"google.golang.org/grpc"
	"testing"
)

func TestReportInfoByReportCode(t *testing.T) {
	url := "120.37.177.122:14001"
	//url = "localhost:9301"
	options := grpcs.DefaultOptions()
	options = append(options, grpc.WithChainUnaryInterceptor(grpc_middleware.ChainUnaryClient(

		grpc_zap.UnaryClientInterceptor(loggers.NewLogger("", "", zapcore.InfoLevel, loggers.MODE_CONSOLE)),
	)))
	conn, err := grpc.Dial(url, options...)
	assert.Nil(t, err)
	rpcClient := bi.NewBiRpcClient(conn)
	ctx := context.Background()

	result, err := rpcClient.ReportInfoByReportCode(ctx, &bi.ReportInfoReq{
		ReportNo: "loc.egn.enterpriseVeh.page",
	})
	assert.Nil(t, err)
	fmt.Println(result)
}
