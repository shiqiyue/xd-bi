package main

import (
	"fmt"
	"gitee.com/lailonghui/vehicle-supervision-framework/pkg/asserts"
	"github.com/xwb1989/sqlparser"
)

func main() {

	sql := "select name as c, apple, now() as currentTime from tableA"
	statement, err := sqlparser.Parse(sql)
	asserts.Nil(err, err)
	s, ok := statement.(*sqlparser.Select)
	if !ok {
		panic("not support sql")
	}
	var columns = []string{}
	for _, expr := range s.SelectExprs {
		a, ok := expr.(*sqlparser.AliasedExpr)
		if ok {
			if !a.As.IsEmpty() {
				columns = append(columns, a.As.String())
			} else {
				colName, ok := a.Expr.(*sqlparser.ColName)
				if ok {
					columns = append(columns, colName.Name.String())
				}
			}

		}
	}
	fmt.Println(columns)
}
