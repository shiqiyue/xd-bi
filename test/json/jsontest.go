package main

import (
	"encoding/json"
	"fmt"
)

var s = `
{"operationName":null,"variables":{"req":{"ReportId":"3319218269409792","Sql":"select \n veh_info.license_plate_number,\n veh_info.license_plate_color,\n driver_info.driver_name,\n org_organization.organization_name,\n temp_driving_log_info.*\nfrom (\n select \n  *\n from \n  driving_log_info\n where\n  vehicle_id in (\n   select \n    veh_id\n   from \n    veh_info\n   where \n    enterprise_id in (\n     select \n      organization_id \n     from \n      org_organization \n     where \n      1=1 \n      {{if eq .auths_DataAccess 2}}\n                   and province_id = @auths_ProvinceId\n               {{end}}\n               {{if eq .auths_DataAccess 3}}\n                   and city_id = @auths_CityId\n               {{end}}\n               {{if eq .auths_DataAccess 4}}\n                   and district_id = @auths_AreaId\n               {{end}}\n    )\n  )\n) as temp_driving_log_info\nleft join \n veh_info \nON \n temp_driving_log_info.vehicle_id = veh_info.veh_id\nleft join \n driver_info \nON \n temp_driving_log_info.driver_id = driver_info.driver_id\nleft join \n org_organization\nON \n veh_info.enterprise_id = org_organization.organization_id","CountSql":"select\n  count(*)\nfrom\n  org_organization o\n  left join org_organization_person p on o.organization_id = p.organization_id\nwhere\n  1=1\n{{if .auths_UserId }}\n    and o.created_at = @auths_UserId\n{{end}}\n{{if .auths_OrganizationIds }}\n    and o.organization_id in @auths_OrganizationIds\n{{end}}\n","DbId":"3319197486862848","SupportGoTemplate":true}},"query":"mutation ($req: UpdateReportSql!) {\n  updateReportSql(Req: $req)\n}\n"}
`

func main() {
	m := map[string]interface{}{}
	err := json.Unmarshal([]byte(s), &m)
	fmt.Println(err)
}
