package main

import (
	"bytes"
	"context"
	"fmt"
	"gitee.com/lailonghui/vehicle-supervision-framework/pkg/asserts"
	"gitee.com/lailonghui/vehicle-supervision-framework/pkg/auths"
	"github.com/antonmedv/expr"
	"text/template"
)

func main() {
	bgCtx := context.Background()
	user := auths.GetUser(bgCtx)
	sql := `
	select
	id ,
	apple
	from t_house
	where
	1=1
	{{if eq .auths.DataAccess 5}}
	and c = #{apple}
	{{end}}
`
	t, err := template.New("t1").Parse(sql)
	asserts.Nil(err, err)
	buffer := &bytes.Buffer{}
	err = t.Execute(buffer, map[string]interface{}{"auths": user})
	asserts.Nil(err, err)
	fmt.Println(buffer.String())
}

func complie1() {
	env := map[string]interface{}{
		"greet":   "Hello, %v!",
		"names":   []string{"world", "you"},
		"sprintf": fmt.Sprintf, // You can pass any functions.
	}

	code := `sprintf(greet, names[0])`

	// Compile code into bytecode. This step can be done once and program may be reused.
	// Specify environment for type check.
	program, err := expr.Compile(code, expr.Env(env))
	if err != nil {
		panic(err)
	}

	output, err := expr.Run(program, env)
	if err != nil {
		panic(err)
	}

	fmt.Print(output)
}
