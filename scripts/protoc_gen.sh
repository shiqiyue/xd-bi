#!/bin/bash
protoFileDir=../api/adm
goOutDir=../../
goGrpcOut=../../
protoFileName=enterprise.proto
cd $protoFileDir
protoc --go_out=$goOutDir $protoFileName --go-grpc_out=$goGrpcOut
